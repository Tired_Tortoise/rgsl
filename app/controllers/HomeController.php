<?php

App::bind('SuccessEngine\Billing\BillingInterface', 'SuccessEngine\Billing\StripeBilling');

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	protected $app;

	public function __construct()
	{
		$this->app = Infusionsoft::sdk();
	}


	public function index()
	{
		return Redirect::to('http://checkout.rgsl.ca/step_one');
	}

	public function step_one()
	{
		Session::put('start', true);

		return View::make('checkout.step_one');
	}

	public function step_two()
	{
		if(Session::has('start')){
			//validate data submitted
			$validation = Validator::make(Input::all(), ['first_name' => 'required', 'last_name' => 'required', 'email' => 'required']);

			//return to form if validation fails
			if($validation->fails())
			{
				return Redirect::back()->withInput()->withErrors($validation->messages());
			}

			Session::put('customer_first_name', Input::get('first_name'));
			Session::put('customer_last_name', Input::get('last_name'));
			Session::put('customer_email', Input::get('email'));
			Session::put('student_count', Input::get('students'));
			Session::put('course_type', Input::get('course_type'));

			//create customer as a contact in infusionsoft
			$id =  $this->app->addWithDupCheck(array('Email' => Input::get('email')), 'Email');

			$result = $this->app->optIn(Input::get('email'),"New Student");

			//save id for checkout
			Session::put('customer_id', $id);

			//Update all the info
			$contact_data = array('FirstName' => Input::get('first_name'), 'LastName' => Input::get('last_name'), 'MiddleName' => Input::get('middle_name'));
			$result = $this->app->updateCon($id, $contact_data);

			//add tag to contact in case they abandon the cart tag=104
			$tagId = 104;
			$result = $this->app->grpAssign($id, $tagId);

			$courses = Course::all();

			$students = CourseStudent::all();

			foreach($courses as $course)
			{
				$course->available = $course->inventory;
				foreach ($students as $student)
				{
					if ($student->course_id == $course->id)
						$course->available -= 1;
				}
			}


			return View::make('checkout.step_two', array('courses' => $courses));

		}else{
			return Redirect::action('HomeController@step_one');
		}


	}

	public function retryTwo()
	{
		if(Session::has('start')){
			$courses = Course::all();

			$students = CourseStudent::all();

			foreach($courses as $course)
			{
				$course->available = $course->inventory;
				foreach ($students as $student)
				{
					if ($student->course_id == $course->id)
						$course->available -= 1;
				}
			}


			return View::make('checkout.step_two', array('courses' => $courses));

		}else{
			return Redirect::action('HomeController@step_one');
		}

	}


	public function step_three($id)
	{
		if(Session::has('start')){
			if(! Session::has('course_id')){
				$validation = Validator::make(Input::all(),['Fees' => 'accepted'], ['accepted' => "Please check to agree that you've read this statement about the RCMP fees!"]);

                //need to add custom validation message 'Please check to agree that you've read this statement about the RCMP fees!'

				//return to form if validation fails
				if($validation->fails())
				{
					return Redirect::back()->withInput()->withErrors($validation->messages());
				}
			}


			//save course to cookies
			Session::put('course_id', $id);

			//get all the courses for the dropdown hidden field
			$courses = Course::all();

			//clean the data for a form
			foreach($courses as $course)
			{
				$clean_courses[$course->id] = $course->label;
			}

			//
			$students = array();

			//array of countries
			$countries = array(0 => '', "Canada" => "Canada", "United States" => "United States", "Afghanistan" => "Afghanistan","Albania" => "Albania","Algeria" => "Algeria","American Samoa" => "American Samoa","Andorra" => "Andorra","Angola" => "Angola","Anguilla" => "Anguilla","Antarctica" => "Antarctica","Antigua and Barbuda" => "Antigua and Barbuda","Argentina" => "Argentina","Armenia" => "Armenia","Aruba" => "Aruba","Australia" => "Australia","Austria" => "Austria","Azerbaijan" => "Azerbaijan","Bahamas" => "Bahamas","Bahrain" => "Bahrain","Bangladesh" => "Bangladesh","Barbados" => "Barbados","Belarus" => "Belarus","Belgium" => "Belgium","Belize" => "Belize","Benin" => "Benin","Bermuda" => "Bermuda","Bhutan" => "Bhutan","Bolivia" => "Bolivia","Bosnia and Herzegowina" => "Bosnia and Herzegowina","Botswana" => "Botswana","Bouvet Island" => "Bouvet Island","Brazil" => "Brazil","British Indian Ocean Territory" => "British Indian Ocean Territory","Brunei Darussalam" => "Brunei Darussalam","Bulgaria" => "Bulgaria","Burkina Faso" => "Burkina Faso","Burundi" => "Burundi","Cambodia" => "Cambodia","Cameroon" => "Cameroon","Cape Verde" => "Cape Verde","Cayman Islands" => "Cayman Islands","Central African Republic" => "Central African Republic","Chad" => "Chad","Chile" => "Chile","China" => "China","Christmas Island" => "Christmas Island","Cocos (Keeling) Islands" => "Cocos (Keeling) Islands","Colombia" => "Colombia","Comoros" => "Comoros","Congo" => "Congo","Congo, the Democratic Republic of the" => "Congo, the Democratic Republic of the","Cook Islands" => "Cook Islands","Costa Rica" => "Costa Rica","Cote d'Ivoire" => "Cote d'Ivoire","Croatia (Hrvatska)" => "Croatia (Hrvatska)","Cuba" => "Cuba","Cyprus" => "Cyprus","Czech Republic" => "Czech Republic","Denmark" => "Denmark","Djibouti" => "Djibouti","Dominica" => "Dominica","Dominican Republic" => "Dominican Republic","East Timor" => "East Timor","Ecuador" => "Ecuador","Egypt" => "Egypt","El Salvador" => "El Salvador","Equatorial Guinea" => "Equatorial Guinea","Eritrea" => "Eritrea","Estonia" => "Estonia","Ethiopia" => "Ethiopia","Falkland Islands (Malvinas)" => "Falkland Islands (Malvinas)","Faroe Islands" => "Faroe Islands","Fiji" => "Fiji","Finland" => "Finland","France" => "France","France Metropolitan" => "France Metropolitan","French Guiana" => "French Guiana","French Polynesia" => "French Polynesia","French Southern Territories" => "French Southern Territories","Gabon" => "Gabon","Gambia" => "Gambia","Georgia" => "Georgia","Germany" => "Germany","Ghana" => "Ghana","Gibraltar" => "Gibraltar","Greece" => "Greece","Greenland" => "Greenland","Grenada" => "Grenada","Guadeloupe" => "Guadeloupe","Guam" => "Guam","Guatemala" => "Guatemala","Guinea" => "Guinea","Guinea-Bissau" => "Guinea-Bissau","Guyana" => "Guyana","Haiti" => "Haiti","Heard and Mc Donald Islands" => "Heard and Mc Donald Islands","Holy See (Vatican City State)" => "Holy See (Vatican City State)","Honduras" => "Honduras","Hong Kong" => "Hong Kong","Hungary" => "Hungary","Iceland" => "Iceland","India" => "India","Indonesia" => "Indonesia","Iran (Islamic Republic of)" => "Iran (Islamic Republic of)","Iraq" => "Iraq","Ireland" => "Ireland","Israel" => "Israel","Italy" => "Italy","Jamaica" => "Jamaica","Japan" => "Japan","Jordan" => "Jordan","Kazakhstan" => "Kazakhstan","Kenya" => "Kenya","Kiribati" => "Kiribati","Korea, Democratic People's Republic of" => "Korea, Democratic People's Republic of","Korea, Republic of" => "Korea, Republic of","Kuwait" => "Kuwait","Kyrgyzstan" => "Kyrgyzstan","Lao, People's Democratic Republic" => "Lao, People's Democratic Republic","Latvia" => "Latvia","Lebanon" => "Lebanon","Lesotho" => "Lesotho","Liberia" => "Liberia","Libyan Arab Jamahiriya" => "Libyan Arab Jamahiriya","Liechtenstein" => "Liechtenstein","Lithuania" => "Lithuania","Luxembourg" => "Luxembourg","Macau" => "Macau","Macedonia, The Former Yugoslav Republic of" => "Macedonia, The Former Yugoslav Republic of","Madagascar" => "Madagascar","Malawi" => "Malawi","Malaysia" => "Malaysia","Maldives" => "Maldives","Mali" => "Mali","Malta" => "Malta","Marshall Islands" => "Marshall Islands","Martinique" => "Martinique","Mauritania" => "Mauritania","Mauritius" => "Mauritius","Mayotte" => "Mayotte","Mexico" => "Mexico","Micronesia, Federated States of" => "Micronesia, Federated States of","Moldova, Republic of" => "Moldova, Republic of","Monaco" => "Monaco","Mongolia" => "Mongolia","Montserrat" => "Montserrat","Morocco" => "Morocco","Mozambique" => "Mozambique","Myanmar" => "Myanmar","Namibia" => "Namibia","Nauru" => "Nauru","Nepal" => "Nepal","Netherlands" => "Netherlands","Netherlands Antilles" => "Netherlands Antilles","New Caledonia" => "New Caledonia","New Zealand" => "New Zealand","Nicaragua" => "Nicaragua","Niger" => "Niger","Nigeria" => "Nigeria","Niue" => "Niue","Norfolk Island" => "Norfolk Island","Northern Mariana Islands" => "Northern Mariana Islands","Norway" => "Norway","Oman" => "Oman","Pakistan" => "Pakistan","Palau" => "Palau","Panama" => "Panama","Papua New Guinea" => "Papua New Guinea","Paraguay" => "Paraguay","Peru" => "Peru","Philippines" => "Philippines","Pitcairn" => "Pitcairn","Poland" => "Poland","Portugal" => "Portugal","Puerto Rico" => "Puerto Rico","Qatar" => "Qatar","Reunion" => "Reunion","Romania" => "Romania","Russian Federation" => "Russian Federation","Rwanda" => "Rwanda","Saint Kitts and Nevis" => "Saint Kitts and Nevis","Saint Lucia" => "Saint Lucia","Saint Vincent and the Grenadines" => "Saint Vincent and the Grenadines","Samoa" => "Samoa","San Marino" => "San Marino","Sao Tome and Principe" => "Sao Tome and Principe","Saudi Arabia" => "Saudi Arabia","Senegal" => "Senegal","Seychelles" => "Seychelles","Sierra Leone" => "Sierra Leone","Singapore" => "Singapore","Slovakia (Slovak Republic)" => "Slovakia (Slovak Republic)","Slovenia" => "Slovenia","Solomon Islands" => "Solomon Islands","Somalia" => "Somalia","South Africa" => "South Africa","South Georgia and the South Sandwich Islands" => "South Georgia and the South Sandwich Islands","Spain" => "Spain","Sri Lanka" => "Sri Lanka","St. Helena" => "St. Helena","St. Pierre and Miquelon" => "St. Pierre and Miquelon","Sudan" => "Sudan","Suriname" => "Suriname","Svalbard and Jan Mayen Islands" => "Svalbard and Jan Mayen Islands","Swaziland" => "Swaziland","Sweden" => "Sweden","Switzerland" => "Switzerland","Syrian Arab Republic" => "Syrian Arab Republic","Taiwan, Province of China" => "Taiwan, Province of China","Tajikistan" => "Tajikistan","Tanzania, United Republic of" => "Tanzania, United Republic of","Thailand" => "Thailand","Togo" => "Togo","Tokelau" => "Tokelau","Tonga" => "Tonga","Trinidad and Tobago" => "Trinidad and Tobago","Tunisia" => "Tunisia","Turkey" => "Turkey","Turkmenistan" => "Turkmenistan","Turks and Caicos Islands" => "Turks and Caicos Islands","Tuvalu" => "Tuvalu","Uganda" => "Uganda","Ukraine" => "Ukraine","United Arab Emirates" => "United Arab Emirates","United Kingdom" => "United Kingdom", "United States Minor Outlying Islands" => "United States Minor Outlying Islands","Uruguay" => "Uruguay","Uzbekistan" => "Uzbekistan","Vanuatu" => "Vanuatu","Venezuela" => "Venezuela","Vietnam" => "Vietnam","Virgin Islands (British)" => "Virgin Islands (British)","Virgin Islands (U.S.)" => "Virgin Islands (U.S.)","Wallis and Futuna Islands" => "Wallis and Futuna Islands","Western Sahara" => "Western Sahara","Yemen" => "Yemen","Yugoslavia" => "Yugoslavia","Zambia" => "Zambia","Zimbabwe" => "Zimbabwe");

			$states =  array(
				'0' => '',
				"SK" => "Saskatchewan",
				"AB" => "Alberta",
				"BC" => "British Columbia",
				"MB" => "Manitoba",
				"NB" => "New Brunswick",
				"NL" => "Newfoundland and Labrador",
				"NT" => "Northwest Territories",
				"NS" => "Nova Scotia",
				"NU" => "Nunavut",
				"ON" => "Ontario",
				"PE" => "Prince Edward Island",
				"QC" => "Quebec",
				"YT" => "Yukon Territory",
				'AL'=>'Alabama',
				'AK'=>'Alaska',
				'AZ'=>'Arizona',
				'AR'=>'Arkansas',
				'CA'=>'California',
				'CO'=>'Colorado',
				'CT'=>'Connecticut',
				'DE'=>'Delaware',
				'DC'=>'District of Columbia',
				'FL'=>'Florida',
				'GA'=>'Georgia',
				'HI'=>'Hawaii',
				'ID'=>'Idaho',
				'IL'=>'Illinois',
				'IN'=>'Indiana',
				'IA'=>'Iowa',
				'KS'=>'Kansas',
				'KY'=>'Kentucky',
				'LA'=>'Louisiana',
				'ME'=>'Maine',
				'MD'=>'Maryland',
				'MA'=>'Massachusetts',
				'MI'=>'Michigan',
				'MN'=>'Minnesota',
				'MS'=>'Mississippi',
				'MO'=>'Missouri',
				'MT'=>'Montana',
				'NE'=>'Nebraska',
				'NV'=>'Nevada',
				'NH'=>'New Hampshire',
				'NJ'=>'New Jersey',
				'NM'=>'New Mexico',
				'NY'=>'New York',
				'NC'=>'North Carolina',
				'ND'=>'North Dakota',
				'OH'=>'Ohio',
				'OK'=>'Oklahoma',
				'OR'=>'Oregon',
				'PA'=>'Pennsylvania',
				'RI'=>'Rhode Island',
				'SC'=>'South Carolina',
				'SD'=>'South Dakota',
				'TN'=>'Tennessee',
				'TX'=>'Texas',
				'UT'=>'Utah',
				'VT'=>'Vermont',
				'VA'=>'Virginia',
				'WA'=>'Washington',
				'WV'=>'West Virginia',
				'WI'=>'Wisconsin',
				'WY'=>'Wyoming',
				'XX'=>'Other...'
			);

			if(Session::has('first_name_student_1'))
			{
				for($i = 0; $i < Session::get('student_count'); $i++)
				{
					$infusionsoft_id = Session::get('student_id_' . $i);

					$returnFields = array('Id','FirstName','LastName', 'Email', 'MiddleName', 'Phone1', 'Birthday', 'StreetAddress1', 'State', 'PostalCode', 'Country', '_Gender', '_FirearmsExperience');
					$query = array('Id' => (int)$infusionsoft_id);
					$students[$i] = $this->app->dsQuery("Contact",1,0,$query,$returnFields);

				}
				return View::make('checkout.step_three_edit', array('courses' => $clean_courses, 'countries' => $countries, 'students' => $students, 'states' => $states));

			}

			return View::make('checkout.step_three', array('courses' => $clean_courses, 'countries' => $countries, 'states' => $states));
		}else{
			return Redirect::action('HomeController@step_one');
		}


	}

	public function step_four()
	{
		if(Session::has('start')){
			//for loop to loop through the number of students
			for($i = 1; $i <= Session::get('student_count');$i++)
			{

				$birthday = Input::get('birth_month_student_' . $i) . '/' . Input::get('birth_day_student_' . $i) . '/'  . Input::get('birth_year_student_' . $i);

				$birthday = strtotime($birthday);

				$birthday = date('Y-m-d', $birthday);

				if(Input::get('country_student_' . $i) == 'Canada'){
					//validate data submitted for each student
					$validation = Validator::make(['postal_code_student_' . $i => Input::get('postal_code_student_' . $i)], ['postal_code_student_' . $i => ['required', 'Regex:/^[A-Za-z]\d[A-Za-z][ ]?\d[A-Za-z]\d$/']], ['regex' => 'Invalid Postal/Zip Code']);

					//return to form if validation fails
					if($validation->fails())
					{
						return Redirect::back()->withInput()->withErrors($validation->messages());
					}
				}

				if(Input::get('country_student_' . $i) == 'United States'){
					$validation = Validator::make(['postal_code_student_' . $i => Input::get('postal_code_student_' . $i)], ['postal_code_student_' . $i => 'required|digits:5'], ['digits' => 'Invalid Postal/Zip Code']);

					//return to form if validation fails
					if($validation->fails())
					{
						return Redirect::back()->withInput()->withErrors($validation->messages());
					}
				}
				//validate data submitted for each student
				$validation = Validator::make(Input::all(),
					['first_name_student_' . $i => 'required',
						'last_name_student_' . $i => 'required',
						'email_student_' . $i => 'required',
						'phone_student_' . $i => 'required|between: 10,15',
						'address_student_' . $i => 'required',
						'state_student_' . $i => 'required',
						'country_student_' . $i => 'required',
						'gender_student_' . $i => 'required',
                        'city_student_' . $i => 'required',
						'experience_student_' . $i => 'required'], ['between' => 'Invalid Phone Number', 'required' => 'This field is required']);

				//return to form if validation fails
				if($validation->fails())
				{
					return Redirect::back()->withInput()->withErrors($validation->messages());
				}

				//add each student to the cookies so that we can create the records after checkout has cleared
				Session::put('first_name_student_' . $i, Input::get('first_name_student_' . $i));
				Session::put('last_name_student_'. $i, Input::get('last_name_student_'. $i));
				if(Input::has('middle_name_student_' . $i))
				{
					Session::put('middle_name_student_' . $i, Input::get('middle_name_student_' . $i));
				}
				Session::put('email_student_' . $i, Input::get('email_student_' . $i));
				Session::put('phone_student_' . $i, Input::get('phone_student_' . $i));
				Session::put('birthday_student_'. $i, $birthday);
				Session::put('address_student_'. $i, Input::get('address_student_' . $i));
                Session::put('city_student_' . $i, Input::get('city_student_' . $i));
				if(Input::get('state_student_' . $i) == 'XX')
				{
					Session::put('state_student_' . $i, Input::get('state_student_' . $i));
					Session::put('state_student_other_' . $i, Input::get('state_student_other_' . $i));
				}else{
					Session::put('state_student_' . $i, Input::get('state_student_' . $i));
				}
				Session::put('postal_code_student_' . $i, Input::get('postal_code_student_' . $i));
				Session::put('country_student_' . $i, Input::get('country_student_' . $i));
				Session::put('gender_student_' . $i, Input::get('gender_student_' . $i));
				Session::put('experience_student_' . $i, Input::get('experience_student_' . $i));
				if(Input::has('company_student_' . $i))
				{
					Session::put('company_student_' . $i, Input::get('company_student_' . $i));
				}
				if(Input::has('twitter_student_' . $i))
				{
					Session::put('twitter_student_' . $i, Input::get('twitter_student_' . $i));
				}

			}


			$course = Course::find(Session::get('course_id'));

			return View::make('checkout.step_four', array('course' => $course));
		}else{
			return Redirect::action('HomeController@step_one');
		}

	}

	public function retryFour(){

		if(Session::has('start')){
			$course = Course::find(Session::get('course_id'));

			return View::make('checkout.step_four', array('course' => $course));
		}else{
			return Redirect::action('HomeController@step_one');
		}
	}

	public function step_five()
	{
		if(Session::has('start')){
			$validation = Validator::make(Input::all(), ['Terms' => 'accepted'], ['accepted' => "Please check to agree that you’ve read and agree to these RGSL policies!"]);

            //need to add custom validation message 'Please check to agree that you’ve read and agree to these RGSL policies!

			if($validation->fails())
			{
				return Redirect::back()->withInput()->withErrors($validation->messages());
			}

			$course = Course::find(Session::get('course_id'));

            //gift code nonsense
            if(Session::has('giftcode_count')){
                $studentsToCharge = Session::get('student_count') - Session::get('giftcode_count');

            }else{
                $studentsToCharge = Session::get('student_count');
            }

            if(Session::get('student_count') >= $course->student_discount && $course->student_discount != 0)
            {
                $price = ($course->discount_price * $studentsToCharge * 100);
            }else {
                $price = ($course->price * $studentsToCharge * 100);
            }

            Session::put('price', $price);

			return View::make('checkout.step_five', ['course' => $course, 'total' => $price]);
		}else{
			return Redirect::action('HomeController@step_one');
		}
	}

	public function step_six()
	{
		if(Session::has('start')){
			$studentNames = '';

			$id = (int)Session::get('customer_id');

			$course = Course::find(Session::get('course_id'));

            //gift code nonsense
            if(Session::has('giftcode_count')){
                $studentsToCharge = Session::get('student_count') - Session::get('giftcode_count');

            }else{
                $studentsToCharge = Session::get('student_count');
            }

			if(Session::get('student_count') >= $course->student_discount && $course->student_discount != 0)
			{
				$price = ($course->discount_price * $studentsToCharge * 100);
			}else {
				$price = ($course->price * $studentsToCharge * 100);
			}

			Session::put('price', $price);

			//check to make sure their is enough space still left in the class
			$course = Course::find(Session::get('course_id'));

			$students = CourseStudent::all();

			$courseDate = strtotime($course->date);

			$courseDate = date('Ymd\TH:i:s', $courseDate);

			$course->available = $course->inventory;
			foreach ($students as $student)
			{
				if ($student->course_id == $course->id)
					$course->available -= 1;
			}

			if($course->available < Session::get('student_count'))
			{
				//if courses are full send to
				return View::make('checkout.full');
			}

			$billing = App::make('SuccessEngine\Billing\BillingInterface');

			$charge = $billing->charge([
				'email' => Session::get('customer_email'),
				'token' => Input::get('stripe-token'),
				'price' => $price
			]);

			//If the card is not valid then we return to the last step with error
			if(isset($charge['message']))
			{
				return View::make('checkout.step_five', array('charge' => $charge, 'course' => $course));
			}

			//add students to infusionsoft and create their record in database
			for($i = 1; $i <= Session::get('student_count');$i++)
			{
				//add the student to infusionsoft if not a contact
				$id =  $this->app->addWithDupCheck(array('FirstName' => Session::get('first_name_student_' . $i), 'Email' => Session::get('email_student_' . $i)), 'EmailAndName');

				//create the state in case they selected the other option
				if(Session::get('state_student_' . $i) == 'XX')
				{
					$state = Session::get('state_student_other_' . $i);
				}else{
					$state = Session::get('state_student_' . $i);
				}

				//add contact data to the new contact
				$contact_data = array('FirstName' => Session::get('first_name_student_' . $i),'LastName' => Session::get('last_name_student_' . $i), 'Email' => Session::get('email_student_' . $i), 'Phone1' => Session::get('phone_student_' . $i), 'Birthday' => Session::get('birthday_student_' . $i), 'StreetAddress1' => Session::get('address_student_' . $i),'City' => Session::get('city_student_' . $i) , 'State' => $state, 'PostalCode' => Session::get('postal_code_student_' . $i), 'Country' => Session::get('country_student_' . $i), '_Gender' => Session::get('gender_student_' . $i), '_FirearmsExperience' => Session::get('experience_student_' . $i), '_CourseDate' => $courseDate, '_CourseLocation0' => $course->location);
				$result = $this->app->updateCon($id, $contact_data);

				$email = Session::get('email_student_' . $i);
				//opt-in email address
				$result = $this->app->optIn($email ,"New Student");

				//add tag to students
				$tagId = 108;
				$result = $this->app->grpAssign($id, $tagId);

				//add unique course tag to students
				$tagId = $course->tag_id;
				$result = $this->app->grpAssign($id, $tagId);

				//add location tag
				$tagId = $course->location_tag;
				$result = $this->app->grpAssign($id, $tagId);

				//save their infusionsoft id to the cookies
				Session::put('student_id_' . $i, $result);

				//now we need to duplicate check to make sure that we aren't adding a student to course they have already registered for
				$records = CourseStudent::all();

				$copy_students = array();

				foreach( $records as $record)
				{
					if($record->infusionsoft_id == $id && $record->course_id == $course->id )
					{
						$copy_students[] = $record->infusionsoft_id;
					}
				}

				if(isset($copy_students[0]))
				{
					//get all the courses for the dropdown hidden field
					$courses = Course::all();

					//clean the data for a form
					foreach($courses as $course)
					{
						$clean_courses[$course->id] = $course->label;
					}

					// Send to page where they are notified that one of the students
					return View::make('checkout.fail', array('error' => $copy_students));

				}

				$student =  new CourseStudent;

				$student->course_id = $course->id;
				$student->infusionsoft_id = $id;

				$student->save();

                if(Session::get('student_count') == $i)
                {
                    $studentNames .= Session::get('first_name_student_' . $i) . ' ' . Session::get('last_name_student_' . $i);

                }else{
                    $studentNames .= Session::get('first_name_student_' . $i) . ' ' . Session::get('last_name_student_' . $i) . ', ';
                }
			}

			$id = (int)Session::get('customer_id');

			$price = '$' . ($price / 100);

			//add location tag
			$tagId = $course->location_tag;
			$result = $this->app->grpAssign($id, $tagId);

			//add custom fields to the user for email
			$contact_data = array( '_CourseDate' => $courseDate, '_CourseLocation0' => $course->location, '_FeePaid' => $price, '_StudentCount' => Session::get('student_count'), '_StudentNames' => $studentNames);
			$result = $this->app->updateCon($id, $contact_data);

			//add tag to user to show that they have completed checkout
			$tagId = 106;
			$result = $this->app->grpAssign($id, $tagId);

			return View::make('checkout.step_six', ['course' => $course]);

		}else{
			return Redirect::action('HomeController@step_one');
		}



	}

	public function printable()
	{
		$course = Course::find(Session::get('course_id'));

		return View::make('checkout.printable', ['course' => $course]);

        //

	}

    public function addGiftCode($gc)
    {
        $codes = GiftCode::all();

        $happened = false;

        //make sure we have not applied to many codes
        if(Session::get('giftcode_count') == Session::get('student_count')){
            return "fail";
        }

        foreach($codes as $code)
        {
            if($code->code == $gc && $code->course_id == 0)
            {
                if(Session::has('giftcode_count'))
                {
                    $count = Session::get('giftcode_count') + 1;

                    //set the session variab;e
                    Session::put('giftcode_count', $count);
                }else{
                    //set the session variab;e
                    Session::put('giftcode_count', 1);
                }

                $redeemedCode = GiftCode::find($code->id);

                //count the code as redeemed
                $redeemedCode->date_redeemed = date('Y-m-d');
                $redeemedCode->course_id = Session::get('course_id');
                $redeemedCode->user_inf_id  = Session::get('customer_id');

                $redeemedCode->save();

                $happened = true;
            }
        }

        if($happened)
        {
            return "success";
        }else{
            return "fail";
        }

    }

}

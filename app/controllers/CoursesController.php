<?php

class CoursesController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

    protected $app;


    public function index()
	{

        $students = CourseStudent::all();

        $courses = Course::all();

        foreach($courses as $course)
        {
            $course->available = $course->inventory;
                foreach ($students as $student)
                {
                    if ($student->course_id == $course->id)
                    $course->available -= 1;
                }
        }

        return View::make('admin.courses', array('courses' => $courses));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('admin.create_course');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $this->app = Infusionsoft::sdk();

        $input = Input::all();

        $date = Input::get('course_month') . '/' . Input::get('course_day') . '/'  . Input::get('course_year');

        //create tag name
        $tagName = $input['label'] . '-' . $date;

        $date = strtotime($date);

        $date = date('Y-m-d', $date);

        //create new infusionsoft tag
        $tagData = array('GroupName' => $tagName);
        $groupIdID = $this->app->dsAdd("ContactGroup", $tagData);

        if ($input['location'] == "Cabela's Saskatoon")
        {
            $tagId = 120;
        }elseif($input['location'] == 'RGSL Classroom')
        {
            $tagId = 118;
        }else
        {
            $tagId = 122;
        }

        $course = new Course;

        $course->label = $input['label'];
        $course->date = $date;
        $course->inventory = $input['inventory'];
        $course->type = $input['type'];
        $course->description = $input['short_description'];
        $course->price = $input['price'];
        $course->time = $input['time'];
        $course->location = $input['location'];
        $course->discount_price = $input['discount_price'];
        $course->student_discount = $input['student_discount'];
        $course->tag_id = $groupIdID;
        $course->location_tag = $tagId;
        $course->code_type = $input['code_type'];

        $course->save();

        return Redirect::route('courses.index');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        return 'Hello show';

    }


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $course = Course::find($id);

        return View::make('admin.edit_course', ['course' => $course]);

    }


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $input = Input::all();

        //convert date string to date for mysql
        $date = Input::get('course_month') . '/' . Input::get('course_day') . '/'  . Input::get('course_year');

        $date = strtotime($date);

        $date = date('Y-m-d', $date);

        $course = Course::find($id);

        if ($input['location'] == "Cabela's Saskatoon")
        {
            $tagId = 120;
        }elseif($input['location'] == 'RGSL Classroom')
        {
            $tagId = 118;
        }else
        {
            $tagId = 122;
        }

        $course->label = $input['label'];
        $course->date = $date;
        $course->inventory = $input['inventory'];
        $course->type = $input['type'];
        $course->description = $input['short_description'];
        $course->price = $input['price'];
        $course->time = $input['time'];
        $course->location = $input['location'];
        $course->discount_price = $input['discount_price'];
        $course->student_discount = $input['student_discount'];
        $course->location_tag = $tagId;


        $course->save();

        return Redirect::route('courses.index');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$course = Course::find($id);

        $course->delete();

        header('Content-type: application/json');
        $response_array['status'] = 'success';
        echo json_encode($response_array);
	}

    public function directory($id)
    {

        $this->app = Infusionsoft::sdk();


        $course = Course::find($id);

        $ruffStudents = CourseStudent::all();

        $students = array();

        if(isset($ruffStudents[0]))
        {
            foreach($ruffStudents as $student)
            {
                if ($student->course_id == $course->id)
                {
                    array_push($students, $student);
                }
            }

            $ids = [];

            foreach($students as $student)
            {
                array_push($ids, (int)$student->infusionsoft_id);
            }

            $returnFields = array('Id','FirstName','LastName', 'Email', 'Phone1');
            $query = array('Id' => $ids);
            $contacts = $this->app->dsQuery("Contact",1000,0,$query,$returnFields);
        }else{
            return View::make('admin.course_students', ['course' => $course,'students' => 0]);
        }

        if(!isset($students))
        {
            return View::make('admin.course_students', ['course' => $course,'students' => 0]);
        }




        return View::make('admin.course_students', ['course' => $course, 'students' => $contacts]);
    }

}

<?php

class UsersController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $users = User::all();

		return View::make('admin.users', array('users' => $users));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('admin.create_user');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

        $validation = Validator::make(Input::all(), ['email' => 'required', 'password' => 'required', 'password2' => 'required']);

        if($validation->fails())
        {
            return Redirect::back()->withInput()->withErrors($validation->messages());
        }

        $email = Input::get('email');

        $password = Input::get('password');

        $password2 = Input::get('password2');

        if($password != $password2)
        {
            return View::make('admin.create_user', ['error' => 'Please make sure your passwords match.']);
        }

        $user = new User;

        $user->email = $email;

        $user->password = Hash::make($password);

        $user->save();

        return Redirect::route('users.index');

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::find($id);

        return View::make('admin.users_show', ['users' => $user]);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $user = User::find($id);

        return View::make('admin.edit_user', ['user' => $user]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $user = User::find($id);

        $validation = Validator::make(Input::all(), ['email' => 'required', 'password' => 'required', 'password2' => 'required']);

        if($validation->fails())
        {
            return Redirect::back()->withInput()->withErrors($validation->messages());
        }

        $email = Input::get('email');

        $password = Input::get('password');

        $password2 = Input::get('password2');

        if($password != $password2)
        {
            return View::make('admin.edit_user', ['error' => 'Please make sure your passwords match.']);
        }

        $user->email = $email;

        $user->password = Hash::make($password);

        $user->save();

        return Redirect::route('users.index');

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::find($id);

        $user->delete();

		header('Content-type: application/json');
		$response_array['status'] = 'success';
		echo json_encode($response_array);

        return Redirect::route('users.index');
	}


}

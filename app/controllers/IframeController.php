<?php

class IframeController extends \BaseController
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $courses = Course::all();

        $students = CourseStudent::all();

        foreach($courses as $course)
        {
            $course->available = $course->inventory;
            foreach ($students as $student) {
                if ($student->course_id == $course->id)
                    $course->available -= 1;
            }

            //convert date for each course
            $date = strtotime($course->date);
            $date = date('l, M d, Y', $date);

            $course->date = $date;
        }





		return View::make('admin.iframe', ['courses' => $courses]);
	}


}
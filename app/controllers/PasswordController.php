<?php

App::bind('SuccessEngine\Billing\BillingInterface', 'SuccessEngine\Billing\StripeBilling');

use Illuminate\Database\Eloquent\ModelNotFoundException;


class PasswordController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    protected $app;

    public function __construct()
    {
        $this->app = Infusionsoft::sdk();
    }

    public function index()
    {
        return View::make('admin.pass');
    }

    public function sendPass()
    {
        $user = User::where('email', '=', Input::get('email'))->firstOrFail();

        //generate random pass
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        $password = implode($pass); //turn the array into a string

        $id = $this->app->addWithDupCheck(array( 'Email' => Input::get('email')), 'Email');

        $result = $this->app->optIn(Input::get('email'),"Password Recovery");

        $user->password = Hash::make($password);

        $user->save();

        $clist = array($id);
        $body = "Your temporary password is " . $password . '. You may keep this as your password but we recommend you login and change it.';
        $status = $this->app->sendEmail($clist,"contact@rgsl.ca","~Contact.Email~", "","","Text","Password Reminder","",$body,"3388");

        return View::make('admin.login', ['pass' => 'Your temporary password has been sent you your email address.']);

    }
}

App::error(function(ModelNotFoundException $e)
{
    $message = 'Email not Found. Please check your email and try again.';
    return View::make('admin.pass', ['message'=> $message]);
});
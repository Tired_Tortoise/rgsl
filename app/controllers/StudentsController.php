<?php


use SuccessEngine\Services\RegisterStudentService;


class StudentsController extends \BaseController {

    protected $app;

    protected $registerStudentService;

    public function __construct(RegisterStudentService $registerStudentService)
    {
        $this->registerStudentService = $registerStudentService;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $this->app = Infusionsoft::sdk();

        $students = CourseStudent::all();

        $contacts = 0;

        if(isset($students[0]))
        {
            $ids = [];

            foreach($students as $student)
            {
                array_push($ids, (int)$student->infusionsoft_id);
            }

            $returnFields = array('Id','FirstName','LastName', 'Email', 'Phone1');
            $query = array('Id' => $ids);
            $contacts = $this->app->dsQuery("Contact",1000,0,$query,$returnFields);

            $courses = Course::all();

            $i = 0;

            foreach($contacts as $contact)
            {
                foreach($students as $student)
                {
                    //assign the course Ids to their correct contacts
                    if($contact['Id'] == $student->infusionsoft_id)
                    {
                        $contacts[$i]['course_id'][] = $student->course_id;
                    }
                }

                foreach($courses as $course)
                {
                    foreach($contacts[$i]['course_id'] as $course_id)
                    {
                        if($course_id == $course->id )
                        {
                            //add the course names by matching the ids we just added to the contact
                            $contacts[$i]['course_names'][] = $course->date;
                        }
                    }
                }
                $i++;
            }
        }

        return View::make('admin.students', ['students' => $contacts]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $courses = Course::all();

        $clean_courses[0] = '';

        foreach($courses as $course)
        {
            $date = strtotime($course->date);

            $date = date('M d, Y', $date);

            $clean_courses[$course->id] = $course->label . ' - ' . $date;
        }

        $countries = array(0 => '',"Canada" => "Canada","United States" => "United States", "Afghanistan" => "Afghanistan","Albania" => "Albania","Algeria" => "Algeria","American Samoa" => "American Samoa","Andorra" => "Andorra","Angola" => "Angola","Anguilla" => "Anguilla","Antarctica" => "Antarctica","Antigua and Barbuda" => "Antigua and Barbuda","Argentina" => "Argentina","Armenia" => "Armenia","Aruba" => "Aruba","Australia" => "Australia","Austria" => "Austria","Azerbaijan" => "Azerbaijan","Bahamas" => "Bahamas","Bahrain" => "Bahrain","Bangladesh" => "Bangladesh","Barbados" => "Barbados","Belarus" => "Belarus","Belgium" => "Belgium","Belize" => "Belize","Benin" => "Benin","Bermuda" => "Bermuda","Bhutan" => "Bhutan","Bolivia" => "Bolivia","Bosnia and Herzegowina" => "Bosnia and Herzegowina","Botswana" => "Botswana","Bouvet Island" => "Bouvet Island","Brazil" => "Brazil","British Indian Ocean Territory" => "British Indian Ocean Territory","Brunei Darussalam" => "Brunei Darussalam","Bulgaria" => "Bulgaria","Burkina Faso" => "Burkina Faso","Burundi" => "Burundi","Cambodia" => "Cambodia","Cameroon" => "Cameroon","Cape Verde" => "Cape Verde","Cayman Islands" => "Cayman Islands","Central African Republic" => "Central African Republic","Chad" => "Chad","Chile" => "Chile","China" => "China","Christmas Island" => "Christmas Island","Cocos (Keeling) Islands" => "Cocos (Keeling) Islands","Colombia" => "Colombia","Comoros" => "Comoros","Congo" => "Congo","Congo, the Democratic Republic of the" => "Congo, the Democratic Republic of the","Cook Islands" => "Cook Islands","Costa Rica" => "Costa Rica","Cote d'Ivoire" => "Cote d'Ivoire","Croatia (Hrvatska)" => "Croatia (Hrvatska)","Cuba" => "Cuba","Cyprus" => "Cyprus","Czech Republic" => "Czech Republic","Denmark" => "Denmark","Djibouti" => "Djibouti","Dominica" => "Dominica","Dominican Republic" => "Dominican Republic","East Timor" => "East Timor","Ecuador" => "Ecuador","Egypt" => "Egypt","El Salvador" => "El Salvador","Equatorial Guinea" => "Equatorial Guinea","Eritrea" => "Eritrea","Estonia" => "Estonia","Ethiopia" => "Ethiopia","Falkland Islands (Malvinas)" => "Falkland Islands (Malvinas)","Faroe Islands" => "Faroe Islands","Fiji" => "Fiji","Finland" => "Finland","France" => "France","France Metropolitan" => "France Metropolitan","French Guiana" => "French Guiana","French Polynesia" => "French Polynesia","French Southern Territories" => "French Southern Territories","Gabon" => "Gabon","Gambia" => "Gambia","Georgia" => "Georgia","Germany" => "Germany","Ghana" => "Ghana","Gibraltar" => "Gibraltar","Greece" => "Greece","Greenland" => "Greenland","Grenada" => "Grenada","Guadeloupe" => "Guadeloupe","Guam" => "Guam","Guatemala" => "Guatemala","Guinea" => "Guinea","Guinea-Bissau" => "Guinea-Bissau","Guyana" => "Guyana","Haiti" => "Haiti","Heard and Mc Donald Islands" => "Heard and Mc Donald Islands","Holy See (Vatican City State)" => "Holy See (Vatican City State)","Honduras" => "Honduras","Hong Kong" => "Hong Kong","Hungary" => "Hungary","Iceland" => "Iceland","India" => "India","Indonesia" => "Indonesia","Iran (Islamic Republic of)" => "Iran (Islamic Republic of)","Iraq" => "Iraq","Ireland" => "Ireland","Israel" => "Israel","Italy" => "Italy","Jamaica" => "Jamaica","Japan" => "Japan","Jordan" => "Jordan","Kazakhstan" => "Kazakhstan","Kenya" => "Kenya","Kiribati" => "Kiribati","Korea, Democratic People's Republic of" => "Korea, Democratic People's Republic of","Korea, Republic of" => "Korea, Republic of","Kuwait" => "Kuwait","Kyrgyzstan" => "Kyrgyzstan","Lao, People's Democratic Republic" => "Lao, People's Democratic Republic","Latvia" => "Latvia","Lebanon" => "Lebanon","Lesotho" => "Lesotho","Liberia" => "Liberia","Libyan Arab Jamahiriya" => "Libyan Arab Jamahiriya","Liechtenstein" => "Liechtenstein","Lithuania" => "Lithuania","Luxembourg" => "Luxembourg","Macau" => "Macau","Macedonia, The Former Yugoslav Republic of" => "Macedonia, The Former Yugoslav Republic of","Madagascar" => "Madagascar","Malawi" => "Malawi","Malaysia" => "Malaysia","Maldives" => "Maldives","Mali" => "Mali","Malta" => "Malta","Marshall Islands" => "Marshall Islands","Martinique" => "Martinique","Mauritania" => "Mauritania","Mauritius" => "Mauritius","Mayotte" => "Mayotte","Mexico" => "Mexico","Micronesia, Federated States of" => "Micronesia, Federated States of","Moldova, Republic of" => "Moldova, Republic of","Monaco" => "Monaco","Mongolia" => "Mongolia","Montserrat" => "Montserrat","Morocco" => "Morocco","Mozambique" => "Mozambique","Myanmar" => "Myanmar","Namibia" => "Namibia","Nauru" => "Nauru","Nepal" => "Nepal","Netherlands" => "Netherlands","Netherlands Antilles" => "Netherlands Antilles","New Caledonia" => "New Caledonia","New Zealand" => "New Zealand","Nicaragua" => "Nicaragua","Niger" => "Niger","Nigeria" => "Nigeria","Niue" => "Niue","Norfolk Island" => "Norfolk Island","Northern Mariana Islands" => "Northern Mariana Islands","Norway" => "Norway","Oman" => "Oman","Pakistan" => "Pakistan","Palau" => "Palau","Panama" => "Panama","Papua New Guinea" => "Papua New Guinea","Paraguay" => "Paraguay","Peru" => "Peru","Philippines" => "Philippines","Pitcairn" => "Pitcairn","Poland" => "Poland","Portugal" => "Portugal","Puerto Rico" => "Puerto Rico","Qatar" => "Qatar","Reunion" => "Reunion","Romania" => "Romania","Russian Federation" => "Russian Federation","Rwanda" => "Rwanda","Saint Kitts and Nevis" => "Saint Kitts and Nevis","Saint Lucia" => "Saint Lucia","Saint Vincent and the Grenadines" => "Saint Vincent and the Grenadines","Samoa" => "Samoa","San Marino" => "San Marino","Sao Tome and Principe" => "Sao Tome and Principe","Saudi Arabia" => "Saudi Arabia","Senegal" => "Senegal","Seychelles" => "Seychelles","Sierra Leone" => "Sierra Leone","Singapore" => "Singapore","Slovakia (Slovak Republic)" => "Slovakia (Slovak Republic)","Slovenia" => "Slovenia","Solomon Islands" => "Solomon Islands","Somalia" => "Somalia","South Africa" => "South Africa","South Georgia and the South Sandwich Islands" => "South Georgia and the South Sandwich Islands","Spain" => "Spain","Sri Lanka" => "Sri Lanka","St. Helena" => "St. Helena","St. Pierre and Miquelon" => "St. Pierre and Miquelon","Sudan" => "Sudan","Suriname" => "Suriname","Svalbard and Jan Mayen Islands" => "Svalbard and Jan Mayen Islands","Swaziland" => "Swaziland","Sweden" => "Sweden","Switzerland" => "Switzerland","Syrian Arab Republic" => "Syrian Arab Republic","Taiwan, Province of China" => "Taiwan, Province of China","Tajikistan" => "Tajikistan","Tanzania, United Republic of" => "Tanzania, United Republic of","Thailand" => "Thailand","Togo" => "Togo","Tokelau" => "Tokelau","Tonga" => "Tonga","Trinidad and Tobago" => "Trinidad and Tobago","Tunisia" => "Tunisia","Turkey" => "Turkey","Turkmenistan" => "Turkmenistan","Turks and Caicos Islands" => "Turks and Caicos Islands","Tuvalu" => "Tuvalu","Uganda" => "Uganda","Ukraine" => "Ukraine","United Arab Emirates" => "United Arab Emirates","United Kingdom" => "United Kingdom","United States Minor Outlying Islands" => "United States Minor Outlying Islands","Uruguay" => "Uruguay","Uzbekistan" => "Uzbekistan","Vanuatu" => "Vanuatu","Venezuela" => "Venezuela","Vietnam" => "Vietnam","Virgin Islands (British)" => "Virgin Islands (British)","Virgin Islands (U.S.)" => "Virgin Islands (U.S.)","Wallis and Futuna Islands" => "Wallis and Futuna Islands","Western Sahara" => "Western Sahara","Yemen" => "Yemen","Yugoslavia" => "Yugoslavia","Zambia" => "Zambia","Zimbabwe" => "Zimbabwe");

        $states =  array(
            '0' => '',
            "SK" => "Saskatchewan",
            "BC" => "British Columbia",
            "ON" => "Ontario",
            "NL" => "Newfoundland and Labrador",
            "NS" => "Nova Scotia",
            "PE" => "Prince Edward Island",
            "NB" => "New Brunswick",
            "QC" => "Quebec",
            "MB" => "Manitoba",
            "AB" => "Alberta",
            "NT" => "Northwest Territories",
            "NU" => "Nunavut",
            "YT" => "Yukon Territory",
            'AL'=>'Alabama',
            'AK'=>'Alaska',
            'AZ'=>'Arizona',
            'AR'=>'Arkansas',
            'CA'=>'California',
            'CO'=>'Colorado',
            'CT'=>'Connecticut',
            'DE'=>'Delaware',
            'DC'=>'District of Columbia',
            'FL'=>'Florida',
            'GA'=>'Georgia',
            'HI'=>'Hawaii',
            'ID'=>'Idaho',
            'IL'=>'Illinois',
            'IN'=>'Indiana',
            'IA'=>'Iowa',
            'KS'=>'Kansas',
            'KY'=>'Kentucky',
            'LA'=>'Louisiana',
            'ME'=>'Maine',
            'MD'=>'Maryland',
            'MA'=>'Massachusetts',
            'MI'=>'Michigan',
            'MN'=>'Minnesota',
            'MS'=>'Mississippi',
            'MO'=>'Missouri',
            'MT'=>'Montana',
            'NE'=>'Nebraska',
            'NV'=>'Nevada',
            'NH'=>'New Hampshire',
            'NJ'=>'New Jersey',
            'NM'=>'New Mexico',
            'NY'=>'New York',
            'NC'=>'North Carolina',
            'ND'=>'North Dakota',
            'OH'=>'Ohio',
            'OK'=>'Oklahoma',
            'OR'=>'Oregon',
            'PA'=>'Pennsylvania',
            'RI'=>'Rhode Island',
            'SC'=>'South Carolina',
            'SD'=>'South Dakota',
            'TN'=>'Tennessee',
            'TX'=>'Texas',
            'UT'=>'Utah',
            'VT'=>'Vermont',
            'VA'=>'Virginia',
            'WA'=>'Washington',
            'WV'=>'West Virginia',
            'WI'=>'Wisconsin',
            'WY'=>'Wyoming',
            'XX'=>'Other...'
        );


        return View::make('admin.create_student', array('courses' => $clean_courses, 'countries' => $countries, 'states' => $states));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $this->app = Infusionsoft::sdk();


        $input = Input::all();

        $validation = Validator::make(Input::all(), ['first_name' => 'required', 'last_name' => 'required', 'email' => 'required', 'phone' => 'required', 'address' => 'required', 'state' => 'required', 'postal_code' => 'required', 'country' => 'required', 'gender' => 'required', 'experience' => 'required', 'city' => 'required']);

        if($validation->fails())
        {
            return Redirect::back()->withInput()->withErrors($validation->messages());
        }

        //add the student to infusionsoft if not a contact
        $id =  $this->app->addWithDupCheck(array('FirstName' => $input['first_name'], 'Email' => $input['email']), 'EmailAndName');


        $birthday = Input::get('birth_month') . '/' . Input::get('birth_day') . '/'  . Input::get('birth_year');

        //add contact data to the new contact
        $contact_data = array('FirstName' => $input['first_name'], 'LastName' => $input['last_name'], 'Email' => $input['email'], 'Phone1' => $input['phone'], 'Birthday' => $birthday, 'StreetAddress1' => $input['address'], 'State' => $input['state'], 'PostalCode' => $input['postal_code'], 'Country' => $input['country'], '_Gender' => $input['gender'], '_FirearmsExperience' => $input['experience'], 'City' => $input['city']);
        $result = $this->app->updateCon($id, $contact_data);

        $record = CourseStudent::where('infusionsoft_id', $id);

        $course = Course::find($input['course']);

        $email = $input['email'];
        //opt-in email address
        $result = $this->app->optIn($email ,"New Student");

        //add tag to students
        $tagId = 108;
        $result = $this->app->grpAssign($id, $tagId);

        //add unique course tag to students
        $tagId = $course->tag_id;
        $result = $this->app->grpAssign($id, $tagId);

        //add location tag
        $tagId = $course->location_tag;
        $result = $this->app->grpAssign($id, $tagId);

        if(isset($record->course_id))
        {

        }else{
            $student =  new CourseStudent;

            $student->course_id = $input['course'];
            $student->infusionsoft_id = $id;

            $student->save();

            return Redirect::to('students');
        }

            return $result;

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

        $this->app = Infusionsoft::sdk();

        $students = CourseStudent::all();

        $current = [];

        foreach($students as $student)
        {
            if($student->infusionsoft_id == $id)
            {
                $current['course_id'] = $student->course_id;
            }
        }

        $returnFields = array('Id','FirstName','LastName', 'Email', 'MiddleName', 'Phone1', 'Birthday', 'StreetAddress1', 'City', 'State', 'PostalCode', 'Country', '_Gender', '_FirearmsExperience');
        $query = array('Id' => (int)$id);
        $student = $this->app->dsQuery("Contact",1,0,$query,$returnFields);

        $courses = Course::all();

        $clean_courses[0] = '';

        foreach($courses as $course)
        {
            $date = strtotime($course->date);

            $date = date('M d, Y', $date);

            $clean_courses[$course->id] = $course->label . ' - ' . $date;
        }

        $countries = array(0 => '', "Canada" => "Canada", "United States" => "United States", "Afghanistan" => "Afghanistan","Albania" => "Albania","Algeria" => "Algeria","American Samoa" => "American Samoa","Andorra" => "Andorra","Angola" => "Angola","Anguilla" => "Anguilla","Antarctica" => "Antarctica","Antigua and Barbuda" => "Antigua and Barbuda","Argentina" => "Argentina","Armenia" => "Armenia","Aruba" => "Aruba","Australia" => "Australia","Austria" => "Austria","Azerbaijan" => "Azerbaijan","Bahamas" => "Bahamas","Bahrain" => "Bahrain","Bangladesh" => "Bangladesh","Barbados" => "Barbados","Belarus" => "Belarus","Belgium" => "Belgium","Belize" => "Belize","Benin" => "Benin","Bermuda" => "Bermuda","Bhutan" => "Bhutan","Bolivia" => "Bolivia","Bosnia and Herzegowina" => "Bosnia and Herzegowina","Botswana" => "Botswana","Bouvet Island" => "Bouvet Island","Brazil" => "Brazil","British Indian Ocean Territory" => "British Indian Ocean Territory","Brunei Darussalam" => "Brunei Darussalam","Bulgaria" => "Bulgaria","Burkina Faso" => "Burkina Faso","Burundi" => "Burundi","Cambodia" => "Cambodia","Cameroon" => "Cameroon","Cape Verde" => "Cape Verde","Cayman Islands" => "Cayman Islands","Central African Republic" => "Central African Republic","Chad" => "Chad","Chile" => "Chile","China" => "China","Christmas Island" => "Christmas Island","Cocos (Keeling) Islands" => "Cocos (Keeling) Islands","Colombia" => "Colombia","Comoros" => "Comoros","Congo" => "Congo","Congo, the Democratic Republic of the" => "Congo, the Democratic Republic of the","Cook Islands" => "Cook Islands","Costa Rica" => "Costa Rica","Cote d'Ivoire" => "Cote d'Ivoire","Croatia (Hrvatska)" => "Croatia (Hrvatska)","Cuba" => "Cuba","Cyprus" => "Cyprus","Czech Republic" => "Czech Republic","Denmark" => "Denmark","Djibouti" => "Djibouti","Dominica" => "Dominica","Dominican Republic" => "Dominican Republic","East Timor" => "East Timor","Ecuador" => "Ecuador","Egypt" => "Egypt","El Salvador" => "El Salvador","Equatorial Guinea" => "Equatorial Guinea","Eritrea" => "Eritrea","Estonia" => "Estonia","Ethiopia" => "Ethiopia","Falkland Islands (Malvinas)" => "Falkland Islands (Malvinas)","Faroe Islands" => "Faroe Islands","Fiji" => "Fiji","Finland" => "Finland","France" => "France","France Metropolitan" => "France Metropolitan","French Guiana" => "French Guiana","French Polynesia" => "French Polynesia","French Southern Territories" => "French Southern Territories","Gabon" => "Gabon","Gambia" => "Gambia","Georgia" => "Georgia","Germany" => "Germany","Ghana" => "Ghana","Gibraltar" => "Gibraltar","Greece" => "Greece","Greenland" => "Greenland","Grenada" => "Grenada","Guadeloupe" => "Guadeloupe","Guam" => "Guam","Guatemala" => "Guatemala","Guinea" => "Guinea","Guinea-Bissau" => "Guinea-Bissau","Guyana" => "Guyana","Haiti" => "Haiti","Heard and Mc Donald Islands" => "Heard and Mc Donald Islands","Holy See (Vatican City State)" => "Holy See (Vatican City State)","Honduras" => "Honduras","Hong Kong" => "Hong Kong","Hungary" => "Hungary","Iceland" => "Iceland","India" => "India","Indonesia" => "Indonesia","Iran (Islamic Republic of)" => "Iran (Islamic Republic of)","Iraq" => "Iraq","Ireland" => "Ireland","Israel" => "Israel","Italy" => "Italy","Jamaica" => "Jamaica","Japan" => "Japan","Jordan" => "Jordan","Kazakhstan" => "Kazakhstan","Kenya" => "Kenya","Kiribati" => "Kiribati","Korea, Democratic People's Republic of" => "Korea, Democratic People's Republic of","Korea, Republic of" => "Korea, Republic of","Kuwait" => "Kuwait","Kyrgyzstan" => "Kyrgyzstan","Lao, People's Democratic Republic" => "Lao, People's Democratic Republic","Latvia" => "Latvia","Lebanon" => "Lebanon","Lesotho" => "Lesotho","Liberia" => "Liberia","Libyan Arab Jamahiriya" => "Libyan Arab Jamahiriya","Liechtenstein" => "Liechtenstein","Lithuania" => "Lithuania","Luxembourg" => "Luxembourg","Macau" => "Macau","Macedonia, The Former Yugoslav Republic of" => "Macedonia, The Former Yugoslav Republic of","Madagascar" => "Madagascar","Malawi" => "Malawi","Malaysia" => "Malaysia","Maldives" => "Maldives","Mali" => "Mali","Malta" => "Malta","Marshall Islands" => "Marshall Islands","Martinique" => "Martinique","Mauritania" => "Mauritania","Mauritius" => "Mauritius","Mayotte" => "Mayotte","Mexico" => "Mexico","Micronesia, Federated States of" => "Micronesia, Federated States of","Moldova, Republic of" => "Moldova, Republic of","Monaco" => "Monaco","Mongolia" => "Mongolia","Montserrat" => "Montserrat","Morocco" => "Morocco","Mozambique" => "Mozambique","Myanmar" => "Myanmar","Namibia" => "Namibia","Nauru" => "Nauru","Nepal" => "Nepal","Netherlands" => "Netherlands","Netherlands Antilles" => "Netherlands Antilles","New Caledonia" => "New Caledonia","New Zealand" => "New Zealand","Nicaragua" => "Nicaragua","Niger" => "Niger","Nigeria" => "Nigeria","Niue" => "Niue","Norfolk Island" => "Norfolk Island","Northern Mariana Islands" => "Northern Mariana Islands","Norway" => "Norway","Oman" => "Oman","Pakistan" => "Pakistan","Palau" => "Palau","Panama" => "Panama","Papua New Guinea" => "Papua New Guinea","Paraguay" => "Paraguay","Peru" => "Peru","Philippines" => "Philippines","Pitcairn" => "Pitcairn","Poland" => "Poland","Portugal" => "Portugal","Puerto Rico" => "Puerto Rico","Qatar" => "Qatar","Reunion" => "Reunion","Romania" => "Romania","Russian Federation" => "Russian Federation","Rwanda" => "Rwanda","Saint Kitts and Nevis" => "Saint Kitts and Nevis","Saint Lucia" => "Saint Lucia","Saint Vincent and the Grenadines" => "Saint Vincent and the Grenadines","Samoa" => "Samoa","San Marino" => "San Marino","Sao Tome and Principe" => "Sao Tome and Principe","Saudi Arabia" => "Saudi Arabia","Senegal" => "Senegal","Seychelles" => "Seychelles","Sierra Leone" => "Sierra Leone","Singapore" => "Singapore","Slovakia (Slovak Republic)" => "Slovakia (Slovak Republic)","Slovenia" => "Slovenia","Solomon Islands" => "Solomon Islands","Somalia" => "Somalia","South Africa" => "South Africa","South Georgia and the South Sandwich Islands" => "South Georgia and the South Sandwich Islands","Spain" => "Spain","Sri Lanka" => "Sri Lanka","St. Helena" => "St. Helena","St. Pierre and Miquelon" => "St. Pierre and Miquelon","Sudan" => "Sudan","Suriname" => "Suriname","Svalbard and Jan Mayen Islands" => "Svalbard and Jan Mayen Islands","Swaziland" => "Swaziland","Sweden" => "Sweden","Switzerland" => "Switzerland","Syrian Arab Republic" => "Syrian Arab Republic","Taiwan, Province of China" => "Taiwan, Province of China","Tajikistan" => "Tajikistan","Tanzania, United Republic of" => "Tanzania, United Republic of","Thailand" => "Thailand","Togo" => "Togo","Tokelau" => "Tokelau","Tonga" => "Tonga","Trinidad and Tobago" => "Trinidad and Tobago","Tunisia" => "Tunisia","Turkey" => "Turkey","Turkmenistan" => "Turkmenistan","Turks and Caicos Islands" => "Turks and Caicos Islands","Tuvalu" => "Tuvalu","Uganda" => "Uganda","Ukraine" => "Ukraine","United Arab Emirates" => "United Arab Emirates","United Kingdom" => "United Kingdom", "United States Minor Outlying Islands" => "United States Minor Outlying Islands","Uruguay" => "Uruguay","Uzbekistan" => "Uzbekistan","Vanuatu" => "Vanuatu","Venezuela" => "Venezuela","Vietnam" => "Vietnam","Virgin Islands (British)" => "Virgin Islands (British)","Virgin Islands (U.S.)" => "Virgin Islands (U.S.)","Wallis and Futuna Islands" => "Wallis and Futuna Islands","Western Sahara" => "Western Sahara","Yemen" => "Yemen","Yugoslavia" => "Yugoslavia","Zambia" => "Zambia","Zimbabwe" => "Zimbabwe");

        $states =  array(
            '0' => '',
            "SK" => "Saskatchewan",
            "BC" => "British Columbia",
            "ON" => "Ontario",
            "NL" => "Newfoundland and Labrador",
            "NS" => "Nova Scotia",
            "PE" => "Prince Edward Island",
            "NB" => "New Brunswick",
            "QC" => "Quebec",
            "MB" => "Manitoba",
            "AB" => "Alberta",
            "NT" => "Northwest Territories",
            "NU" => "Nunavut",
            "YT" => "Yukon Territory",
            'AL'=>'Alabama',
            'AK'=>'Alaska',
            'AZ'=>'Arizona',
            'AR'=>'Arkansas',
            'CA'=>'California',
            'CO'=>'Colorado',
            'CT'=>'Connecticut',
            'DE'=>'Delaware',
            'DC'=>'District of Columbia',
            'FL'=>'Florida',
            'GA'=>'Georgia',
            'HI'=>'Hawaii',
            'ID'=>'Idaho',
            'IL'=>'Illinois',
            'IN'=>'Indiana',
            'IA'=>'Iowa',
            'KS'=>'Kansas',
            'KY'=>'Kentucky',
            'LA'=>'Louisiana',
            'ME'=>'Maine',
            'MD'=>'Maryland',
            'MA'=>'Massachusetts',
            'MI'=>'Michigan',
            'MN'=>'Minnesota',
            'MS'=>'Mississippi',
            'MO'=>'Missouri',
            'MT'=>'Montana',
            'NE'=>'Nebraska',
            'NV'=>'Nevada',
            'NH'=>'New Hampshire',
            'NJ'=>'New Jersey',
            'NM'=>'New Mexico',
            'NY'=>'New York',
            'NC'=>'North Carolina',
            'ND'=>'North Dakota',
            'OH'=>'Ohio',
            'OK'=>'Oklahoma',
            'OR'=>'Oregon',
            'PA'=>'Pennsylvania',
            'RI'=>'Rhode Island',
            'SC'=>'South Carolina',
            'SD'=>'South Dakota',
            'TN'=>'Tennessee',
            'TX'=>'Texas',
            'UT'=>'Utah',
            'VT'=>'Vermont',
            'VA'=>'Virginia',
            'WA'=>'Washington',
            'WV'=>'West Virginia',
            'WI'=>'Wisconsin',
            'WY'=>'Wyoming',
            'XX'=>'Other...'
        );


        return View::make('admin.edit_student', ['student' => $student, 'countries' => $countries, 'courses' => $clean_courses, 'current' => $current, 'states' => $states]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($id)
	{
        $this->app = Infusionsoft::sdk();

        $input = Input::all();

        $birthday = Input::get('birth_month') . '/' . Input::get('birth_day') . '/'  . Input::get('birth_year');

        //add contact data to the new contact
        $contact_data = array('FirstName' => $input['first_name'], 'LastName' => $input['last_name'], 'Email' => $input['email'], 'Phone1' => $input['phone'], 'Birthday' => $birthday, 'StreetAddress1' => $input['address'], 'State' => $input['state'], 'PostalCode' => $input['postal_code'], 'Country' => $input['country'], '_Gender' => $input['gender'], '_FirearmsExperience' => $input['experience']);
        $result = $this->app->updateCon($id, $contact_data);

        $records = CourseStudent::all();

        $contact = array();

        foreach( $records as $record)
        {
            if($record->infusionsoft_id == $result && $record->course_id == $input['course'] )
            {
                return Redirect::to('students');
            }
        }

        $student =  new CourseStudent;

        $student->course_id = $input['course'];
        $student->infusionsoft_id = $id;

        $student->save();

        return Redirect::to('students');


        print_r($contact);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

        $record =  CourseStudent::where('infusionsoft_id', $id);

        $record->delete();

        header('Content-type: application/json');
        $response_array['status'] = 'success';
        echo json_encode($response_array);
	}


    public function remove($id, $course_id)
    {
        $records = CourseStudent::all();

        foreach($records as $record)
        {
            if($record->course_id == $course_id && $record->infusionsoft_id == $id)
            {
                $record->delete();

                header('Content-type: application/json');
                $response_array['status'] = 'success';
                echo json_encode($response_array);
            }
        }
    }

}

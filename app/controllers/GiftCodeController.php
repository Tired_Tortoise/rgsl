<?php

class GiftCodeController extends \BaseController {

    protected $app;

    public function __construct()
    {
        $this->app = Infusionsoft::sdk();
    }

    /**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $codes = GiftCode::all();

        $courses = Course::all();

        $ids = array();

        foreach($codes as $code)
        {
            array_push($ids, (int)$code->user_inf_id);
        }

        $returnFields = array('Id','FirstName','LastName', 'Email');
        $query = array('Id' => $ids);
        $contacts = $this->app->dsQuery("Contact",1000,0,$query,$returnFields);

        return View::make('admin.gift_codes', ['codes' => $codes, 'contacts' => $contacts, 'courses' => $courses]);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('admin.create_gift_codes');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $randomCode = $this->generateRandomString();

        $codes = GiftCode::all();

        $repeat = true;

        foreach($codes as $code)
        {
            if($code->code == $randomCode)
            {
                $randomCode = $this->generateRandomString();
            }
        }

        $gift_code = new GiftCode();

        $gift_code->code = $randomCode;
        $gift_code->course_type = Input::get('type');

        $gift_code->save();

        return Redirect::to('giftcodes');


	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $code = GiftCode::find($id);

        return View::make('admin.add_note', ['code' => $code]);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $code = GiftCode::find($id);

        $code->note = Input::get('note');

        $code->save();

        return Redirect::to('giftcodes');

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        $code = GiftCode::find($id);

        $code->delete();
	}

    public function generateRandomString() {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 10; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


}

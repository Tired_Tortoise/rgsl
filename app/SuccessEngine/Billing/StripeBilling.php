<?php namespace SuccessEngine\Billing;

use Stripe;
use Stripe_Charge;
use Config;
use Stripe_CardError;
use Stripe_InvalidRequestError;
use Stripe_AuthenticationError;
use Stripe_ApiConnectionError;
use Stripe_Error;
use Exception;


class StripeBilling implements BillingInterface {

    public function __construct()
    {
        Stripe::setApiKey(Config::get('stripe.secret_key'));
    }

    public function charge(array $data)
    {
        try
        {

            return Stripe_Charge::create([
                'amount' => $data['price'],
                'currency' => 'cad',
                'description' => $data['email'],
                'card' => $data['token']
            ]);
        }
        catch(Stripe_CardError $e) {
            // Since it's a decline, Stripe_CardError will be caught
            $body = $e->getJsonBody();
            $err  = $body['error'];
            return $err;
        } catch (Stripe_InvalidRequestError $e) {
            // Invalid parameters were supplied to Stripe's API
        } catch (Stripe_AuthenticationError $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
        } catch (Stripe_ApiConnectionError $e) {
            // Network communication with Stripe failed
        } catch (Stripe_Error $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
        }
    }
}
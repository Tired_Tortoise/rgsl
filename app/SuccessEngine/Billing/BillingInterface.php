<?php namespace SuccessEngine\Billing;


interface BillingInterface {
    public function charge(array $data);
}
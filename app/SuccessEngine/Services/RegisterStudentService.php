<?php

namespace SuccessEngine\Services;

use DB;
use CourseStudent;
use Spoolphiz\Infusionsoft\Facades\Infusionsoft;
use Validator;



class RegisterStudentService {

    private $courseStudent;

    private $infusionsoft;

    private $app;

    private $validator;

    function __construct(CourseStudent $courseStudent, Infusionsoft $infusionsoft, Validator $validator)
    {
        $this->courseStudent = $courseStudent;

        $this->infusionsoft = $infusionsoft;

        $this->app = $infusionsoft::sdk();

        $this->validator = $validator;
    }


    public function registerStudent($data)
    {

        $validation = $this->validator->make($data, ['first_name' => 'required', 'last_name' => 'required', 'email' => 'required', 'phone' => 'required', 'birthday' => 'required', 'address' => 'required', 'state' => 'required', 'postal_code' => 'required', 'country' => 'required', 'gender' => 'required', 'experience' => 'required']);

        if($validation->fails())
        {
            return false;
        }

        //add the student to infusionsoft if not a contact
        $id =  $this->app->addWithDupCheck(array('Email' => $data['email']), 'Email');

        //add contact data to the new contact
        $contact_data = array('FirstName' => $data['first_name'], 'LastName' => $data['last_name'], 'Email' => $data['email'], 'Phone1' => $data['phone'], 'Birthday' => $data['birthday'], 'StreetAddress1' => $data['address'], 'State' => $data['state'], 'PostalCode' => $data['postal_code'], 'Country' => $data['country'], '_Gender' => $data['gender'], '_FirearmsExperience' => $data['experience']);
        $result = $this->app->updateCon($id, $contact_data);

        $record = $this->courseStudent->where('infusionsoft_id', $id);

        if(isset($record->course_id))
        {

        }else{
            $student =  new $this->courseStudent;

            $student->course_id = $data['course'];
            $student->infusionsoft_id = $id;

            $student->save();

            return true;
        }
    }
}
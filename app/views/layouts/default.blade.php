<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="publishable-key" content="{{ Config::get('stripe.publishable_key') }}">

    <title>Online Registration</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::asset('assets/admin/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::asset('assets/admin/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::asset('assets/admin/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ URL::asset('assets/admin/ico/apple-touch-icon-57-precomposed.png') }}">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-32x32.png" sizes="32x32">

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('assets/admin/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- page css files -->
    <link href="{{ URL::asset('assets/admin/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/admin/css/jquery-ui.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/checkout.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('assets/admin/css/style.min.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <!-- Custom CSS -->
    <link href="{{ URL::asset('assets/css/business-casual.css') }}" rel="stylesheet">

    <!-- Fonts -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style rel="stylesheet">
        .form-control{
            width: 90% !important;

        }
        .logo-img{
            display:block;
            margin:auto;
            width: 550px;
            max-width: 70%;
            height: auto;
        }
        .label-danger{
            font-size: 13px !important;
        }
    </style>
    <script src="{{ URL::asset('assets/js/jquery.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#scroll').bind('scroll', chk_scroll);
        });
    </script>
</head>
<body>

@yield('body')


<!-- jQuery -->

<script src="{{ URL::asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery-migrate-1.2.1.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/bootstrap.min.js') }}"></script>


<!-- page scripts -->
<script src="{{ URL::asset('assets/admin/js/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery.sparkline.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery.chosen.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery.cleditor.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery.autosize.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery.placeholder.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery.maskedinput.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery.inputlimiter.1.3.1.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/moment.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/daterangepicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery.hotkeys.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/bootstrap-wysiwyg.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/bootstrap-colorpicker.min.js') }}"></script>

<!-- theme scripts -->
<script src="{{ URL::asset('assets/admin/js/custom.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/core.min.js') }}"></script>

<!-- inline scripts related to this page -->
<script src="{{ URL::asset('assets/admin/js/pages/form-elements.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/pages/table.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('assets/admin/js/dataTables.bootstrap.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.formance.js') }}"></script>
<script src="{{ URL::asset('assets/js/awesome_form.js') }}"></script>

<script src="https://js.stripe.com/v2/"></script>

<script>

    function chk_scroll(e)
    {
        var elem = $(e.currentTarget);
        if (elem.scrollTop()+ elem.innerHeight() >= elem[0].scrollHeight - 20)
        {
            $('.notSecret').hide();
            $('.mySecret').show();
        }

    }

    function changeState(id){
        var state = $('#state_'+id).val();
        if(state == 'XX'){
            $('#other_'+id).show();
            $('#other_'+id).attr('required');
        }
    }

    function submitForm(id) {
        $('#theForm').attr('action', '<?php echo URL::to('step_three') ?>/' + id);

        if($('#Fees').attr('checked')){
            var agree = document.getElementById('Fees');
            agree.setCustomValidity("");
            $('#theForm').submit();
        }
    }

    function customValidate(){
        var agree = document.getElementById('agree-box');
        if(agree.checked) {

            agree.setCustomValidity("");
            return true;
        }else{
            return false;
        }
    }


</script>
@yield('footer')
</body>
</html>
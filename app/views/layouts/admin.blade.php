<!DOCTYPE html>
<html lang="en">

<head>



    <title>@yield('title')</title>
    @include('sections.head')


</head>

<body>
<!-- Navigation -->
@include('sections.admin_header')
<div class="container-fluid content">
        <div class="row">
@include('sections.admin_sidenav')
@yield('head')


<!-- start: Content -->
            <div class="col-md-10 col-sm-11 main ">
                <div class="row">

@yield('content')

            </div>
        </div>
    </div>
</div>
<!-- end: Content -->

    <!-- start: JavaScript-->
    	<!--[if !IE]>-->

    			<script src="{{ URL::asset('assets/admin/js/jquery-2.1.0.min.js') }}"></script>

    	<!--<![endif]-->

    	<!--[if IE]>

    		<script src="<?php echo URL::asset('assets/admin/js/jquery-1.11.0.min.js') ?>"></script>

    	<![endif]-->

    	<!--[if !IE]>-->

    		<script type="text/javascript">
    			window.jQuery || document.write("<script src='<?php echo URL::asset('assets/admin/js/jquery-2.1.0.min.js')?>'>"+"<"+"/script>");
    		</script>

    	<!--<![endif]-->

    	<!--[if IE]>

    		<script type="text/javascript">
    	 	window.jQuery || document.write("<script src='assets/js/jquery-1.11.0.min.js'>"+"<"+"/script>");
    		</script>

    	<![endif]-->
    	<script src="{{ URL::asset('assets/admin/js/jquery-migrate-1.2.1.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/bootstrap.min.js') }}"></script>


    	<!-- page scripts -->
    	<script src="{{ URL::asset('assets/admin/js/jquery-ui.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/jquery.sparkline.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/jquery.chosen.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/jquery.cleditor.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/jquery.autosize.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/jquery.placeholder.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/jquery.maskedinput.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/jquery.inputlimiter.1.3.1.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/bootstrap-datepicker.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/bootstrap-timepicker.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/moment.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/daterangepicker.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/jquery.hotkeys.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/bootstrap-wysiwyg.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/bootstrap-colorpicker.min.js') }}"></script>

    	<!-- theme scripts -->
    	<script src="{{ URL::asset('assets/admin/js/custom.min.js') }}"></script>
    	<script src="{{ URL::asset('assets/admin/js/core.min.js') }}"></script>

    	<!-- inline scripts related to this page -->
    	<script src="{{ URL::asset('assets/admin/js/pages/form-elements.js') }}"></script>
        <script src="{{ URL::asset('assets/admin/js/pages/table.js') }}"></script>
        <script src="{{ URL::asset('assets/admin/js/jquery.dataTables.min.js') }}"></script>
        <script src="{{ URL::asset('assets/admin/js/dataTables.bootstrap.min.js') }}"></script>

        <script type="text/javascript">
        function deleteStudent(id)
        {
            var url = "<?php echo URL::route('students.index')  ?>/" + id;
            var result = confirm("Are you sure you want to delete the Student?");

            if (result == true)
            {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    success: function(response){
                        window.location.replace('<?php echo URL::route('students.index') ?>');
                    }
                });
            }
        }

        function deleteCourse(id)
        {
            var url = "<?php echo URL::route('courses.index')  ?>/" + id;
            var result = confirm("Are you sure you want to delete the course?");

            if (result == true)
            {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    success: function(response){
                        location.reload();
                    }
                });
            }
        }

        function deleteUser(id)
        {
            var url = "<?php echo URL::route('users.index')  ?>/" + id;
            var result = confirm("Are you sure you want to delete the user?");

            if (result == true)
            {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    success: function(response){
                        location.reload();
                    }
                });
            }
        }

        function deleteCode(id)
        {
            var url = "<?php echo URL::route('giftcodes.index')  ?>/" + id;
            var result = confirm("Are you sure you want to delete the code?");

            if (result == true)
            {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    success: function(response){
                        location.reload();
                    }
                });
            }
        }

        </script>


    	<!-- end: JavaScript-->

    @yield('footer')

</body>

</html>
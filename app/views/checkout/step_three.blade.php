@extends('layouts.default')

@section('body')
<div class="container">
    <div class="row">
        <div class="box">
            <div><img class="logo-img" src="{{URL::asset('assets/img/RGSL_Logo.png')}}"></div>
            <div class="col-lg-12">
                <h1 class="text-center"><strong>Student Information</strong></h1>

                <hr>
                @for($i = 0; $i < Session::get('student_count'); $i++)
                    @if(isset($error[$i]))
                    <div>{{ $error[$i] }}</div>
                    @endif
                @endfor
                {{ Form::open(array('url' => URL::action('HomeController@step_four'))) }}
                <fieldset>
                @for($i = 1; $i <= Session::get('student_count'); $i++)
                    <div class="row">
                    <h1 class="text-center"> Student {{ $i }}</h1>
                    <hr style>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('first_name', 'First Name') }}
                                {{ Form::text('first_name_student_' . $i, null, array('class' => 'form-control', 'required', 'placeholder' => 'John')) }}
                                <span class="label label-danger">{{ $errors->first('first_name_student_' .$i) }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('middle_name', 'Middle Name (optional)') }}
                                {{ Form::text('middle_name_student_' . $i, null, array('class' => 'form-control', 'placeholder' => 'Anthony')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('last_name', 'Last Name') }}
                                {{ Form::text('last_name_student_' . $i, null, array('class' => 'form-control', 'required', 'placeholder' => 'Smith')) }}
                                <span class="label label-danger">{{ $errors->first('last_name_student_' . $i) }}</span>

                            </div>
                            <div class="form-group">
                                {{ Form::label('email', 'Email') }}
                                {{ Form::text('email_student_' . $i, null, array('class' => 'form-control email', 'data-formance_algorithm' => 'complex', 'required', 'style' => 'display: inline !important;', 'placeholder' => 'johnsmith@gmail.com')) }}
                                <span class="label label-danger">{{ $errors->first('email_student_' . $i) }}</span>

                            </div>
                            <div class="form-group">
                                {{ Form::label('phone', 'Best Contact Number') }}
                                {{ Form::text('phone_student_' . $i, null, array('class' => 'form-control phone_number', 'required', 'placeholder' => '(306) 555-1234', 'style' => 'display: inline !important;')) }}
                                <span class="label label-danger">{{ $errors->first('phone_student_' . $i) }}</span>

                            </div>
                            <div class="form-group">
                                {{ Form::label('birthday', 'Date-of-Birth') }}
                                <br>
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::selectMonth('birth_month_student_' . $i, date('m'), ['class' => 'form-control']) }}
                                    </div>
                                    <div class="col-sm-2">
                                        {{ Form::selectRange('birth_day_student_' . $i, '1', '31', date('d'), ['class' => 'form-control']) }}
                                    </div>
                                    <div class="col-sm-3">
                                        {{ Form::selectYear('birth_year_student_' . $i, date('Y') - 18, date('Y') - 120, null, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('company', 'Company (optional)') }}
                                {{ Form::text('company_student_' . $i, null, array('class' => 'form-control', 'placeholder' => 'What is your occupation?')) }}
                                <span class="label label-danger">{{ $errors->first('company_student_' . ($i + 1)) }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::hidden('course', Session::get('course_id'))}}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('address', 'Street Address') }}
                                {{ Form::text('address_student_' . $i, null, array('class' => 'form-control', 'required', 'placeholder' => '2405 Legislative Drive')) }}
                                <span class="label label-danger">{{ $errors->first('address_student_' . ($i + 1)) }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('city', 'Town/City') }}
                                {{ Form::text('city_student_' . $i, null, array('class' => 'form-control', 'required', 'placeholder' => 'Regina')) }}
                                <span class="label label-danger">{{ $errors->first('city_student_' . $i) }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('state', 'Province/State') }}
                                {{ Form::select('state_student_' . $i, $states, 'SK', array('class' => 'form-control', 'data-rel' => 'chosen', 'required', 'onchange' => 'changeState('.$i.')', 'id' => 'state_' . $i)) }}
                                {{ Form::text('state_student_other_' . $i, null, array('class' => 'form-control', 'style' => 'display: none', 'id' => 'other_' . $i)) }}
                                <span class="label label-danger">{{ $errors->first('state_student_' . $i) }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('postal_code', 'Postal/Zip Code') }}
                                {{ Form::text('postal_code_student_' . $i, Session::get('postal_code_student_' . $i), array('class' => 'form-control','required', 'style' => 'display: inline !important;', 'placeholder' => 'S4S3B3 or 12345')) }}
                                <span class="label label-danger">{{ $errors->first('postal_code_student_' . $i) }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('country', 'Country') }}
                                {{ Form::select('country_student_' . $i, $countries, 'Canada', array( 'data-rel' => 'chosen','class' => 'form-control', 'required')) }}
                                <span class="label label-danger">{{ $errors->first('country_student_' . $i) }}</span>

                            </div>
                            <div class="form-group">
                                {{ Form::label('gender', 'Gender') }}
                                {{ Form::select('gender_student_' . $i, array('Male' => 'Male', 'Female' => 'Female'), null, array('class' => 'form-control', 'required')) }}
                                <span class="label label-danger">{{ $errors->first('gender_student_' . $i) }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('experience', 'Firearms Experience') }}
                                {{ Form::text('experience_student_' . $i, null, array('class' => 'form-control', 'placeholder' => 'Lots of past experience shooting, or are you just starting out?')) }}
                                <span class="label label-danger">{{ $errors->first('experience_student_' . $i) }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('twitter', 'Twitter (optional)') }}
                                {{ Form::text('twitter_student_' . $i, null, array('class' => 'form-control', 'placeholder' => '@your_twitter_handle')) }}
                            </div>
                        </div>
                        <!-- Change this to a button or input when using this as a form -->

                    </div>
                @endfor
                    <div style="padding-top: 10px">
                        {{ Form::submit('Continue', array('class' => 'btn btn-lg btn-rgsl btn-block', 'required')) }}
                    </div>
                </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@extends('layouts.default')

@section('body')
<div class="container">
    <div class="row">
        <div class="box">
            <div><img class="logo-img" src="{{URL::asset('assets/img/RGSL_Logo.png')}}"></div>
            <div class="col-lg-12">
                <hr>
                <h2 class="intro-text text-center">
                    <strong>Basic information</strong>
                </h2>
                <hr>
                {{ Form::open(array('url' => URL::action('HomeController@step_two'))) }}
                <div class="row">
                    <div class="form-group col-lg-6">
                            {{ Form::label('first_name', 'First Name') }}
                            {{ Form::text('first_name', null, array('class' => 'form-control', 'required')) }}
                            <label class="label label-danger">{{ $errors->first('first_name') }}</label>
                    </div>
                    <div class="form-group col-lg-6">
                        {{ Form::label('middle_name',  'Middle Name') }}
                        {{ Form::text('middle_name', null, array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        {{ Form::label('last_name',  'Last Name') }}
                        {{ Form::text('last_name', null, array('class' => 'form-control', 'required')) }}
                        <label class="label label-danger">{{ $errors->first('last_name') }}</label>
                    </div>
                    <div class="form-group col-lg-6" style="margin-bottom: 0px;">
                        {{ Form::label('email', 'Email') }}
                        {{ Form::email('email', null, array('class' => 'form-control email', 'data-formance_algorithm' => 'complex', 'required', 'style' => 'display: inline !important;')) }}
                        <label class="label label-danger">{{ $errors->first('email') }}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-lg-6">
                        {{ Form::label('course_type', 'Course Type') }}
                        {{ Form::select('course_type', array(
                        'public' => 'Public',
                        'private' => 'Private'), null, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group col-lg-6" >
                        {{ Form::label('students', 'Number of students to register') }}
                        {{ Form::select('students', array(
                            '1' => '1',
                            '2' => '2',
                            '3' => '3',
                            '4' => '4',
                            '5' => '5',
                            '6' => '6',
                            '7' => '7',
                            '8' => '8',
                            '9' => '9',
                            '10' => '10',
                            '11' => '11',
                            '12' => '12',
                            ), null, array('class' => 'form-control')) }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="form-group col-lg-6">
                        {{ Form::submit('Submit', array('class' => 'btn btn-rgsl btn-block')) }}
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
    <!-- /.container -->

@stop
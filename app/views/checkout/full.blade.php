@extends('layouts.default')


@section('body')
    <div class="container">
        <div class="row">
            <div class="box">
                <div><img style="display:block; margin:auto;" class="" height="275px" src="http://rgsl.ca/wp-content/uploads/2014/10/rgsl-oct14-320px.png"></div>
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">Course
                        <strong>Full</strong>
                    </h2>

                    <p>We're sorry but the course you attempted to register for has been filled.</p>
                    <br>
                    <a href="{{ URL::action('HomeController@step_two') }}"><button class="btn btn-lg btn-success">Pick a new Course</button></a>
                </div>
            </div>
        </div>
    </div>
@stop
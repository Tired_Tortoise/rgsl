@extends('layouts.default')

@section('title')
    Online Registration
@stop

@section('body')
    <div class="container">
        <div class="row">
            <div class="box">
                <div><img class="logo-img" src="{{URL::asset('assets/img/RGSL_Logo.png')}}"></div>
                <div class="col-lg-12">
                    <h1 class="text-center">Sorry</h1>
                    <hr>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-8">
                        <h2 class="text-center"> One or more of the students you are trying to register is already in the class. Please email us at CONTACT@RGSL.CA to fix the problem.</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
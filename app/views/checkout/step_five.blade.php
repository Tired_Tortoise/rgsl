@extends('layouts.default')

@section('body')
    <div class="container">
        <div class="row">
            <div class="box">
                <div><img class="logo-img" src="{{URL::asset('assets/img/RGSL_Logo.png')}}"></div>
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="text-center"><strong>CONFIRM YOUR ORDER</strong></h1>
                        <hr>
                        <div class="col-lg-12" style="padding-bottom: 20px">
                            <div class="col-lg-6" style="margin-left: 25%;">
                                <table class="table table-striped">
                                    <thead>
                                    <th>Name</th>
                                    <th>Price</th>
                                    <th>Quantity</th>
                                    <th>Total (tax included)</th>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td><strong>{{ $course->label }}</strong></td>
                                        <td id="price"><strong>$<?php
                                                if(Session::get('student_count') >= $course->student_discount && $course->student_discount != 0)
                                                {
                                                    $price = $course->discount_price;
                                                }else{
                                                    $price = $course->price;
                                                }
                                                echo $price;
                                                ?></strong></td>
                                        <td><strong>{{ Session::get('student_count') }}</strong></td>
                                        <td id="total"><strong>${{ $total/100 }}</strong></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4" style="margin-left: 33%;">
                                <div id="code-success" style="display: none" class="alert alert-success">

                                </div>
                                <div id="code-fail" style="display: none" class="alert alert-danger">

                                </div>
                            </div>
                        </div>

                        {{ Form::open(array('url' => URL::action('HomeController@step_six'), 'id' => 'billing-form')) }}
                        <div class="row">
                            <div class="col-lg-5" style="margin-left: 11%;">
                                <h2>Billing Information</h2>
                                <div class="form-group">
                                    {{ Form::label('first_name', 'First Name') }}
                                    {{ Form::text('first_name', Session::get('customer_first_name'), array('class' => 'form-control')) }}
                                    {{ $errors->first('first_name') }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('last_name', 'Last Name') }}
                                    {{ Form::text('last_name', Session::get('customer_last_name'), array('class' => 'form-control')) }}
                                    {{ $errors->first('last_name') }}
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <h2>Credit Card Information</h2>
                                @if(isset($charge))
                                    <div id="first-message" class="alert alert-danger">{{ $charge['message'] . " Please re-check your card data carefully and try again. If this message persists, contact your card issuer for more details."}}</div>
                                @endif
                                <div hidden="true" class="payment-errors alert alert-danger"></div>
                                <div class="form-group">
                                    {{ Form::label('credit_card_type', 'Credit Card Type') }}
                                    {{ Form::select('card_type', array('American Express' => 'American Express', 'Discover' => 'Discover', 'MasterCard' => 'MasterCard', 'Visa' => 'Visa'), null, array('class' => 'form-control', 'required')) }}
                                    {{ $errors->first('card_type') }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('credit_card_number', 'Credit Card Number') }}
                                    <input type="text" data-stripe="number" required class="form-control" id="" placeholder="0000 0000 0000 0000"/>
                                    {{--{{ Form::text('credit_card_number', null, array('class' => 'form-control', 'id' => 'ccnumber', 'placeholder' => '0000 0000 0000 0000')) }}--}}
                                </div>
                                <div class="row">
                                    <div class="form-group col-sm-4">
                                        {{ Form::label('month', 'Month') }}
                                        {{ Form::selectMonth(null, null, array('class' => 'form-control', 'id' => 'ccmonth', 'data-stripe' => 'exp-month', 'required')) }}
                                        {{ $errors->first('month') }}
                                    </div>
                                    <div class="form-group col-sm-4">
                                        {{ Form::label('year', 'Year') }}
                                        {{ Form::selectYear(null, date('Y'), date('Y') + 10, null, array('class' => 'form-control', 'id' => 'ccyear', 'data-stripe' => 'exp-year', 'required')) }}
                                        {{ $errors->first('year') }}
                                    </div>
                                    <div class="form-group col-sm-4">
                                        {{ Form::label('cvv', 'CVV/CVC') }}
                                        <input type="text" data-stripe="cvc" required class="form-control" id="cvv" placeholder="123"/>
                                        {{--{{ Form::text('cvv', null, array('class' => 'form-control', 'id' => 'cvv')) }}--}}
                                        {{ $errors->first('cvv') }}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-5" style="margin-left: 33%;">
                                <h2>Gift Code</h2>
                                {{ Form::label('code', 'Apply a Gift Code') }}
                                {{ Form::text('code', null, array('class' => 'form-control', 'id' => 'code')) }}

                                <button type="button" onclick="useGiftCode()" style="margin-left: 75%;margin-top: 5px;margin-bottom: 5px;" id="code-button" class="btn btn-sm btn-rgsl">Apply</button>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="form-group col-lg-6" style="margin-left: 25%;">
                        <p class="text-center" style="font-size: 1em"><strong>This is your final step. Confirm your payment to complete your purchase.</strong></p>
                        <input id="sButton" value="Confirm and Pay" type="submit" class="btn btn-block btn-lg btn-rgsl">
                    </div>
                </div>

                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop

@section('footer')
    <script src="{{ URL::asset('assets/js/billing.js') }}" ></script>

    <script>
        function useGiftCode()
        {
            code = $('#code').val();

            var url = "addCode/" + code;

            price = {{ $price }};

            total = {{ $total/100  }};

            if(total == 0){
                $('#code-success').hide();

                $('#code-fail').text('Your total is at $0. You can not add anymore Gift Codes.');
                $('#code-fail').show();

                return;
            }

            $.ajax({
                url: url,
                type: 'GET',
                success: function(response){

                    if(response == 'success'){
                        $('#code-fail').hide();


                        $('#code-success').text('Your Gift Code was applied successfully!');
                        $('#code-success').show();

                        total = total - price;

                        $('#total').html('<strong>$' + total + '</strong>');
                    }else{
                        $('#code-success').hide();

                        $('#code-fail').text('There was a problem applying your gift code. Please check your code and try again. If your problem persists contact us at CONTACT@RGSL.CA.');
                        $('#code-fail').show();
                    }

                }
            });
        }
    </script>
@stop

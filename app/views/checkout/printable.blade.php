<html>
<head>
    <style>
        .text-center{
            text-align: center;
        }
        #content {
            width: auto;
            border: 0;
            margin: 0 5%;
            padding: 0;
            float: none !important;
        }
    </style>
    <script>
        window.onload = function (){
            window.print();
        }
    </script>
</head>
<body>
<div id="content">
    <div class="row">
        <div class="box">
            <div><img style="display:block; margin:auto;" class="" height="275px" src="{{URL::asset('assets/img/RGSL_Logo.png')}}"></div>
            <div class="col-lg-12">
                <h1 class="text-center">Thank You</h1>
                <hr>
                <h2 class="text-center"><strong>RGSL Registration Confirmation/Receipt</strong></h2>
                <h2 class="text-center"><strong>{{ date('G:i m/d/Y', time()-21600) }}</strong></h2>
                <h3 class="text-center">Please print/save this page for your records.</h3>
                <h3 class="text-center">Thank you {{ Session::get('customer_first_name') . ' ' . Session::get('customer_last_name') }}. Your registration was submitted successfully!</h3>
                <h3 class="text-center">This confirms that you have successfully registered {{ Session::get('student_count') }} student(s) with Regina Gun Safety & Licensing!</h3>
                <h4>The students you registered were:</h4>
                @for($i = 1; $i <= Session::get('student_count');$i++)
                    <p>{{ Session::get('first_name_student_' . $i) . ' ' . Session::get('last_name_student_' . $i) }}</p>
                @endfor
                        The fee charged today including taxes was ${{ Session::get('price') / 100 }}.
                        <br/>
                        <br/>
                        @if($course->location == 'RGSL Classroom')
                            This course is being held at RGSL’s private classroom, located at 2700 Montague Street (Lower Level Suite 6) in the basement of the River Heights Mall at {{ $course->date . ' ' . $course->time }}. The Firearms Safety Course will start promptly and on time.
                            <br/>
                            <br/>
                            We will take some short breaks throughout the day - please feel free to bring food and hot/cold drinks as this is a day-long course.
                            <br/>
                            <br/>
                            The River Heights Mall is home to a Mac’s Convenience Store where you can get some snacks and drinks throughout the day. The RGSL private classroom also has a small fridge where you can store food, as well as a microwave where you can warm food up.
                            <br/>
                            <br/>
                            Please ensure on the course date you bring with you one piece of government identification such as your Canadian driver's license, international passport or permanent resident card.
                            <br/>
                            <br/>
                            If you have reading glasses, please bring them along with you as we will be spending time on cartridge and firearm identification during the day (as well as taking written notes).
                            <br/>
                            <br/>
                            If you are using the course as an opportunity to upgrade from a POL (Possession-Only License), or to change from one type of PAL  to another, bring your former license if you are able to do so (it will help us with some of your forms).
                            <br/>
                            <br/>
                            Please take note of the date/time/location of your course, and remember that our Cancellation/Refund Policy, Satisfaction Guaranteed Policy and Student Terms & Conditions can all be found at the RGSL.CA website.
                            <br/>
                            <br/>
                            Thank you for registering, and we look forward to seeing you at the course!
                            <br/>
                            <br/>
                            -RGSL Staff
                        @elseif($course->location == 'Cabela’s Saskatoon')
                            This course is being held at Cabela’s Saskatoon (Conference Room), located at 1714 Preston Avenue N., Saskatoon SK at {{ date('m/d/Y', strtotime($course->date)) . ' ' . $course->time }}. The Firearms Safety Course will start promptly and on time.
                            <br/>
                            <br/>
                            We will take some short breaks throughout the day - please feel free to bring food and hot/cold drinks as this is a day-long course.
                            <br/>
                            <br/>
                            Cabela's Saskatoon also offers various food products for sale, including their famous Fudge Shop.
                            <br/>
                            <br/>
                            Please ensure on the course date you bring with you one piece of government identification such as your Canadian driver's license, international passport or permanent resident card.
                            <br/>
                            <br/>
                            If you have reading glasses, please bring them along with you as we will be spending time on cartridge and firearm identification during the day (as well as taking written notes).
                            <br/>
                            <br/>
                            If you are using the course as an opportunity to upgrade from a POL (Possession-Only License), or to change from one type of PAL  to another, bring your former license if you are able to do so (it will help us with some of your forms).
                            <br/>
                            <br/>
                            Please take note of the date/time/location of your course, and remember that our Cancellation/Refund Policy, Satisfaction Guaranteed Policy and Student Terms & Conditions can all be found at the RGSL.CA website.
                            <br/>
                            <br/>
                            Thank you for registering, and we look forward to seeing you at the course!
                            <br/>
                            <br/>
                            -RGSL Staff
                        @else
                            This course is being held at Cabela’s Regina (Conference Room), located at 4901 Gordon Road, Regina SK at {{ date('m/d/Y', strtotime($course->date)) . ' ' . $course->time }}. The Firearms Safety Course will start promptly and on time.
                            <br/>
                            <br/>
                            We will take some short breaks throughout the day - please feel free to bring food and hot/cold drinks as this is a day-long course.
                            <br/>
                            <br/>
                            Cabela's Regina also offers various food products for sale, including their famous Fudge Shop.
                            <br/>
                            <br/>
                            Please ensure on the course date you bring with you one piece of government identification such as your Canadian driver's license, international passport or permanent resident card.
                            <br/>
                            <br/>
                            If you have reading glasses, please bring them along with you as we will be spending time on cartridge and firearm identification during the day (as well as taking written notes).
                            <br/>
                            <br/>
                            If you are using the course as an opportunity to upgrade from a POL (Possession-Only License), or to change from one type of PAL  to another, bring your former license if you are able to do so (it will help us with some of your forms).
                            <br/>
                            <br/>
                            Please take note of the date/time/location of your course, and remember that our Cancellation/Refund Policy, Satisfaction Guaranteed Policy and Student Terms & Conditions can all be found at the RGSL.CA website.
                            <br/>
                            <br/>
                            Thank you for registering, and we look forward to seeing you at the course!
                            <br/>
                            <br/>
                            -RGSL Staff
                        @endif
            </div>
        </div>
    </div>
</div>
</body>
</html>

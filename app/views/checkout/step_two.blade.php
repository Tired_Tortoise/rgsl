@extends('layouts.default')


@section('body')
    <?php $noCourses = true; ?>
    @foreach($courses as $course)
        @if(strtotime($course->date) > date('U') && $course->available >= Session::get('student_count') && Session::get('course_type') == $course->type)
            <?php $noCourses = false; ?>
        @endif
    @endforeach

            <div class="container">
    <div class="row">
        <div class="box">
            <div class="row">
                <div><img class="logo-img" src="{{URL::asset('assets/img/RGSL_Logo.png')}}"></div>
                <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">
                        <strong>UPCOMING COURSES</strong>
                    </h2>
                    <hr>
                    {{ Form::open(array('url' => '', 'id' => 'theForm', 'method' => 'POST')) }}
                </div>
                <div class="col-lg-3"></div>
                @if($noCourses)
                    <div class="col-lg-6 text-center"></div>
                @else
                    <div class="col-lg-6 text-center">
                        <div>
                            I understand that after the course, the RCMP will charge students a separate application/processing fee of $60-$80 when they mail their form packages for processing. I furthermore understand that the RCMP have sole discretion in approving or denying individual firearms licenses. <input name="Fees" id="Fees" class="" type="checkbox">
                        </div>
                        <span class="label label-danger">{{ $errors->first('Fees') }}</span>
                        <hr>
                    </div>
                @endif
            </div>
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <table class="table">
                        <tbody>
                        <?php
                        $array = array();

                        foreach($courses as $course)
                        {
                            array_push($array, $course);
                        }

                        function date_compare($a, $b)
                        {
                            $t1 = strtotime($a['date']);
                            $t2 = strtotime($b['date']);
                            return $t1 - $t2;
                        }

                        usort($array, 'date_compare');
                        ?>
                        @foreach($array as $course)
                            @if(strtotime($course->date) >= date('U') && $course->available >= Session::get('student_count') && Session::get('course_type') == $course->type)
                                    <tr style="">
                                    <td style="border: 2px solid #747474;padding: 10px;">
                                        <h1 style="font-size: 2em;">{{ $course->label }}</h1>
                                        <h2 class="intro-text"><strong>Seats Left: </strong> {{ $course->available }}</h2>
                                        @if(Session::get('student_count') >= $course->student_discount && $course->student_discount != 0)
                                            <h2 class="intro-text"><strong>Price: </strong> ${{ $course->discount_price }}</h2>
                                        @else
                                            <h2 class="intro-text"><strong>Price: </strong> ${{ $course->price }}</h2>
                                        @endif
                                        <h2 class="intro-text" style=""><strong>Course Date: </strong> {{ $course->time . ' ' .  date('D M d Y', strtotime($course->date)) }}</h2>
                                        <h2 class="intro-text"><strong>Course Location: </strong> {{ $course->location }}</h2>
                                        <p>{{ $course->description }}</p>
                                        <button onclick="submitForm({{ $course->id }})" class="btn btn-md btn-rgsl">Choose Course</button>
                                    </td>
                            @endif
                        @endforeach
                        @if($noCourses && Session::get('course_type') == 'private')
                            <tr>
                                <td>
                                    There are no current private bookings scheduled. To schedule a private booking for you and your group, please <a href="http://rgsl.ca/contact/">contact us</a>. To book in for a public course, please <a href="{{ URL::to('/step_one')  }}">click here</a>.
                                </td>
                            </tr>
                        @elseif($noCourses && Session::get('course_type') == 'public')
                            <tr>
                                <td>
                                    There are no currently scheduled public courses. Please return to our <a href="http://rgsl.ca">home page</a> for more information.
                                </td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
    <!-- /.container -->

@stop
@extends('layouts.default')

@section('body')
    <div class="container">
        <div class="row">
            <div class="box">
                <div><img class="logo-img" src="{{URL::asset('assets/img/RGSL_Logo.png')}}"></div>
                <div class="col-lg-12" style="padding-bottom: 20px">
                    <h1 class="text-center"><strong>Pricing</strong></h1>
                    <hr>
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <table class="table table-striped">
                            <thead>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Total (tax included)</th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><strong>{{ $course->label }}</strong></td>
                                    <td><strong>$<?php
                                            if(Session::get('student_count') >= $course->student_discount && $course->student_discount != 0)
                                            {
                                                $price = $course->discount_price;
                                            }else{
                                                $price = $course->price;
                                            }
                                        echo $price;
                                        ?></strong></td>
                                    <td><strong>{{ Session::get('student_count') }}</strong></td>
                                    <td><strong>$<?php
                                            if(Session::get('student_count') >= $course->student_discount && $course->student_discount != 0)
                                                {
                                                    $total = Session::get('student_count') * $course->discount_price;
                                                }else{
                                                    $total = Session::get('student_count') * $course->price;
                                                }
                                    echo $total;
                                    ?></strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6">
                        <h1 class="text-center">Confirm Student Information</h1>
                        <hr>

                            @for($i = 1; $i <= Session::get('student_count') ; $i++)
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <h1 class=""><strong>Student {{ $i }}</strong></h1>
                                    <div class="">
                                        <p style="font-size: 1em;"><strong>First Name:</strong> {{ Session::get('first_name_student_' . $i);}}</p>
                                        @if(Session::has('middle_name_student_' . $i))
                                            <p style="font-size: 1em;"><strong>Middle Name: </strong> {{ Session::get('middle_name_student_' . $i) }}</p>
                                        @else
                                            <p style="font-size: 1em;> "><strong>Middle Name: </strong></p>
                                        @endif
                                        <p style="font-size: 1em;"><strong>Last Name: </strong> {{ Session::get('last_name_student_' . $i) }}</p>
                                        <p style="font-size: 1em;"><strong>Email: </strong> {{ Session::get('email_student_' . $i) }}</p>
                                        <P style="font-size: 1em;"><strong>Phone: </strong> {{ Session::get('phone_student_' . $i) }}</p>
                                        <p style="font-size: 1em;"><strong>Date-of-Birth: </strong> {{ date('m/d/Y', strtotime(Session::get('birthday_student_' . $i))) }}</p>
                                        <p style="font-size: 1em;"><strong>Street Address: </strong>{{ Session::get('address_student_' . $i) }}</p>
                                        <p style="font-size: 1em;"><strong>Town/City: </strong>{{ Session::get('city_student_' . $i) }}</p>
                                        @if(Session::get('state_student_' . $i) == 'XX')
                                            <p style="font-size: 1em;"><strong>Province/State: </strong>{{ Session::get('state_student_other_' . $i) }}</p>
                                        @else
                                            <p style="font-size: 1em;"><strong>Province/State: </strong>{{ Session::get('state_student_' . $i) }}</p>
                                        @endif
                                        <p style="font-size: 1em;"><strong>Postal Code: </strong>{{ Session::get('postal_code_student_' . $i) }}</p>
                                        <p style="font-size: 1em;"><strong>Country: </strong>{{ Session::get('country_student_' . $i) }}</p>
                                        <p style="font-size: 1em;"><strong>Gender: </strong>{{ Session::get('gender_student_' . $i) }}</p>
                                        <p style="font-size: 1em;"><strong>Firearms Experience: </strong>{{ Session::get('experience_student_' . $i) }}</p>
                                        <p style="font-size: 1em;"><strong>Company: </strong>{{ Session::get('company_student_' . $i) }}</p>
                                        <p style="font-size: 1em;"><strong>Twitter: </strong>{{ Session::get('twitter_student_' . $i) }}</p>

                                    </div>
                                </div>
                                <div class="col-lg-3"></div>
                            </div>
                            @endfor
                            <div class="col-lg-11">
                                <a href="{{ URL::action('HomeController@step_three', Session::get('course_id')) }}"><button class="btn btn-rgsl btn-block">Edit Students</button></a>
                            </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-9" style="margin-left: 12%; margin-right: 10%;">
                        <h2>Please read and agree to the Student Terms & Conditions for the billing party (or parties) and student (or students) of RGSL</h2>
                        <hr>
                        <div class="col-lg-1"></div>
                        <div class="col-lg-10">
                            <div class="row">
                                <div id="scroll" style="max-width: 100%;height:600px;overflow:auto;padding:5px;border: 1px solid black">
                                    <strong style="text-decoration: underline">These are the Terms & Conditions for the billing party (or parties) and student (or students) of RGSL.</strong>
                                    <br>
                                    <br>
                                    <ul>
                                        <li>If a student has previously been prohibited from owning firearms by a court or police order, the possibility exists that they are not allowed to get a Canadian gun license. It is the student’s responsibility to check on their own actual eligibility for a PAL. The best way to do this is by calling the RCMP’s Canadian Firearms Program at 1-800-731-4000 prior to course registration. RGSL will not be held liable if it is found later that the student is ineligible for a license, and it is recommended that students be as honest and open as possible with the RCMP in this process. According to RCMP statistics, 99.5% of applications are processed without incident.</li>
                                        <p></p>
                                        <li>The RCMP will charge each student an application/processing fee after the course is complete. This fee is usually either $60 or $80, and is usually paid with a Visa/Mastercard or with a personal cheque. This application/processing fee is not included in the course’s registration fee. The application/processing fee recurs once every 5 years (The renewal process is much like that of a Canadian Passport) and students do not need to take another course at that time.</li>
                                        <p></p>
                                        <li>Payments for 10-Hour Public Firearms Safety Courses are completed using MasterCard, Visa, or American Express. We only offer online registration for public courses at this time. RGSL’s courses do fill rapidly. Once the course has been completed, RGSL recommends mailing student forms as soon as possible to prevent them from getting lost or damaged. RGSL may keep copies of completed forms on hand, but maintains no guarantee that it will be able to replace forms after the course date passes. Copies of Course Report (JUS332) forms will also have been sent soon after the course to the provincial Saskatchewan RCMP Chief Firearms Officer in Regina S.K. and to the RCMP Central Processing Centre in Miramichi, N.B.</li>
                                        <p></p>
                                        <li>RGSL reserves the right to cancel or modify upcoming courses. Students will be given the option of either refunds or transfers to other courses in the event of a course modification or cancellation.</li>
                                        <p></p>
                                        <li>All students must be 18 years of age on the day of the course. Government-issued photo-ID such as a Driver’s License or Passport is required for some forms to be completed. RGSL does not train shooters who are under the age of 18 at this time.</li>
                                        <p></p>
                                        <li>RGSL keeps a copy of the corporate Privacy Policy at <a target="_blank" href="http://www.rgsl.ca/privacy-policy/">http://www.rgsl.ca/privacy-policy/</a>.</li>
                                    </ul>
                                    <br>
                                    <br>
                                    <strong style="text-decoration: underline">Cancellation & Refund Policy:</strong>
                                    <br>
                                    <br>
                                    Refunds will be issued scarcely, at the discretion of RGSL, and must be received specifically in e-mail format at contact@rgsl.ca by the applicable deadlines in order to have RGSL consider a student for qualification of a refund.
                                    <br>
                                    <br>

                                    Refunds are issued at the sole discretion of RGSL and its agents and maintains the following refund deadlines:
                                    <br>
                                    <br>
                                    <ul>

                                        <li>	If a student submits a request to cancel their reservation via email with more than 7 days’ notice, RGSL shall issue a full refund (or a transfer to a different course if the student prefers).</li>

                                        <li>	If a student submit a request to cancel their reservation via email with more than 72 hours before the start of course but less than 7 days, they will receive a 50% monetary refund.</li>

                                        <li>	If a student requests to cancel within 72 hours of the start time of a course (or fails to show up for a course), they will not receive a refund or be allowed to transfer to a future course free of charge.</li>
                                    </ul>
                                    <br>
                                    Registrations that occur less than 72 hours from the start time of a course are not refundable.
                                    <br>
                                    <br>
                                    <br>
                                    <strong style="text-decoration: underline">Satisfaction Guaranteed Policy:</strong>
                                    <br>
                                    <br>
                                    RGSL guarantees that students will have a premium experience. Here's how it works if a student isn’t happy after one of our courses or challenges:
                                    <br>
                                    <br>

                                    <ul>
                                        <li>	In the event that after a Firearms Safety Course or Challenge a student remains unsatisfied with the education or services provided by RGSL, we ask that they work with us towards a mutual resolution to help complete the educational process. Failing a timely resolution, RGSL will fully refund the billing party and will cancel/destroy any outstanding paperwork applications on the student’s behalf.</li>
                                        <li>	If the student’s paperwork has already been mailed off and was marked with a passing grade, no refund will be provided as that application will have entered the processing stage with the RCMP (and so the student’s benefit will already have been realized).</li>
                                        <li>	RGSL does not refund students who fail the federal written and/or practical testing. RGSL will make every reasonable effort to have expert instructors help students with the material, and re-writes of the tests may be offered RGSL’s discretion (in consultation with the RCMP).</li>
                                        <li>	For any students who do fail a component of the federal testing: there is a mandatory one-week cooldown period before students may register and test again with any instructor.</li>
                                    </ul>
                                    While in a class with RGSL, the student(s) agree that they shall follow any posted or disclaimed course rules, and shall furthermore refrain from using foul or slanderous language or harassing, badgering or soliciting other individuals. In no event shall a student's behaviour, demeanour, hygiene or attitude be in any way offensive, threatening, intimidating, unsanitary or in any other way contrary to the best interest of the host venue, RGSL, or the group being taught. It is furthermore agreed upon that RGSL may dismiss or eject any student without cause at any point in time (a refund shall be at the sole discretion of RGSL).
                                    <br>
                                    <br>
                                    I/we understand that I/we, the billing party(ies) and/or student(s), on behalf of my (our) heirs, assigns, personal representatives and next of kin, do hereby agree to release and hold harmless Regina Gun Safety & Licensing, its officers, agents, and/or employees, other students and releasee(s), sponsoring agencies, and the owner and leaser of the premises used in the operation of Regina Gun Safety & Licensing, with respect to any and all injury, death, disability, death, or loss or damage to person or property, HOWSOEVER CAUSED, INCLUSIVE OF ANY NEGLIGENCE, PAST, PRESENT, OR FUTURE, OF ANY OF YOU, THE RELEASEE(S) AND/OR STUDENT(S).

                                </div>
                            </div>
                            <br>
                            <div class="row"></div>
                            <p class="notSecret">Scroll to the bottom of the Terms & Conditions to continue</p>
                            {{ Form::open(array('url' => URL::action('HomeController@step_five'))) }}
                            <div class="mySecret" style="display: none"><input type="checkbox" name="Terms" id="agree-box"> I Agree to the <a target="_blank" href="http://rgsl.ca/student-terms-conditions/"><strong>Student Terms & Conditions</strong></a>, <a target="_blank" href="http://rgsl.ca/cancellation-refund-policy/"><strong>Cancellation & Refund Policy</strong></a>, and the <a target="_blank" href="http://rgsl.ca/satisfaction-guaranteed-policy/"><strong>Satisfaction Guaranteed Policy</strong></a>.</div>
                            <span class="label label-danger">{{ $errors->first('Terms') }}</span>

                            <button onclick="customValidate()" type="submit" style="display: none" class="btn btn-rgsl pull-right mySecret">Next Step</button>
                            <br>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>

@stop


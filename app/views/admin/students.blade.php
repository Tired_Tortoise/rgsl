@extends('...layouts.admin')


@section('title')
students
@stop

@section('content')


<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="col-lg-10">
                <h2>Students</h2>
            </div>
            <div class="col-lg-2">
                <a style="padding-top: 2px" href="{{ URL::action('StudentsController@create') }}"><button class="btn-sm btn-primary">Create Student</button></a>
            </div>
        </div>
        <div class="panel-body">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Course</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if($students == 0)

                    @else
                        @foreach($students as $student)
                            <tr>
                            <td>
                            @if(isset($student['FirstName']) && isset($student['LastName']))
                                    {{ $student['FirstName'] . ' ' . $student['LastName'] }}
                                    <a href="{{ URL::action('StudentsController@edit', $student['Id']) }}"><button class="btn btn-sm btn-info pull-right"><i class="fa fa-edit"></i></button></a>
                                </td>
                            @elseif(isset($student['FirstName']))
                                    {{ $student['FirstName'] }}
                                <a href="{{ URL::action('StudentsController@edit', $student['Id']) }}"><button class="btn btn-sm btn-info pull-right"><i class="fa fa-edit"></i></button></a>
                                </td>
                            @else
                                <a href="{{ URL::action('StudentsController@edit', $student['Id']) }}"><button class="btn btn-sm btn-info pull-right"><i class="fa fa-edit"></i></button></a>
                                </td>
                            @endif
                            <td>{{ $student['Email'] }}</td>
                            @if(isset($student['Phone1']))
                                <td>{{ $student['Phone1'] }}</td>
                            @else
                                <td></td>
                            @endif
                            <td>
                            @if(isset($student['course_names']))
                                @foreach($student['course_names'] as $name)
                                    {{ $name }},
                                @endforeach
                            @endif
                            {{--<button type="button" class="btn btn-success pull-right"><i class="fa fa-plus"></i></button>--}}
                            </td>
                            <td>
                                <button type="button" onclick="deleteStudent(<?php echo $student['Id'] ?>)" class="btn btn-danger pull-right"><i class="fa fa-times"></i></button>
                            </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>


@stop



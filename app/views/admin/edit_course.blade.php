@extends('...layouts.admin')


@section('title')
Edit Course
@stop


@section('content')
    <?php
    $times = array();

    $start = "0:00";
    $end = "23:30";

    $tStart = strtotime($start);
    $tEnd = strtotime($end);
    $tNow = $tStart;

    while($tNow <= $tEnd){
        $times[date("g:i A",$tNow)] = date("g:i A",$tNow);
        $tNow = strtotime('+30 minutes',$tNow);
    }
    ?>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Edit Course</h3>
            </div>
            <div class="panel-body">
                {{ Form::open(array('url' => URL::action('CoursesController@update', $course->id), 'method' => 'put')) }}
                    <fieldset>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('label', $course->label, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('price', 'Price') }}
                            {{ Form::text('price', $course->price, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('date', 'Course Date') }}
                            <div class="form-group">
                                <div class="col-sm-4">
                                    {{ Form::selectMonth('course_month', date('m', strtotime($course->date)), ['class' => 'form-control']) }}
                                </div>
                                <div class="col-sm-2">
                                    {{ Form::selectRange('course_day', '1', '31', date('d', strtotime($course->date)), ['class' => 'form-control']) }}
                                </div>
                                <div class="col-sm-3">
                                    {{ Form::selectYear('course_year', date('Y'), date('Y') + 120, date('Y', strtotime($course->date)), ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <br>
                        <div style="padding-top: 5px" class="form-group">
                            {{ Form::label('time', 'Course Time') }}
                            {{ Form::select('time', $times,$course->time, array('class' => 'form-control' , 'required')) }}
                        </div>
                        <div class="form-group">
                        {{ Form::label('type', 'Course Type', array('class' => 'control-label', 'for' => 'selectError3')) }}
                            <div class="controls">
                                {{ Form::select('type', ['public' => 'Public', 'private' => 'Private'], $course->type, ['class' => 'form-control', 'id' => 'selectError3']) }}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Change Price to</label>
                            {{ Form::text('discount_price', $course->discount_price, array('class' => 'form-control', 'style' => 'width: 75px; display: inline;')) }}
                            <label class="control-label" style="display: inline">when</label>
                            {{ Form::text('student_discount', $course->student_discount, array('class' => 'form-control', 'style' => 'width: 50px; display: inline;')) }}
                            <label style="display: inline">students are registering.</label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                                {{ Form::label('location', "Location") }}
                                {{ Form::select('location', ["Cabela's Saskatoon" => "Cabela's Saskatoon", 'RGSL Classroom' => 'RGSL Classroom', "Cabela's Regina" => "Cabela's Regina"], $course->location, array('required', 'class' => 'form-control') ) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('inventory', 'Course Seats') }}
                            {{ Form::text('inventory', $course->inventory, array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('code_type', 'Code Type') }}
                            {{ Form::select('code_type', ['1' => 'Non-Restricted','2' => 'Restricted', '3' => 'Non-Restricted/Restricted'], $course->code_type, ['class' => 'form-control', 'required']) }}
                            <span class="label label-danger">{{ $errors->first('code_type') }}</span>
                        </div>
                        <div class="form-group">
                            {{ Form::label('short description', 'Short Description') }}
                            {{ Form::textarea('short_description', $course->description, array('class' => 'form-control')) }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6"></div>
                        <div class="col-lg-3">
                            <button type="button" onclick="deleteCourse(<?php echo $course->id ?>)" class="btn btn-lg btn-danger btn-block">Delete</button>
                        </div>
                        <div class="col-lg-3">
                            {{ Form::submit('Save', array('class' => 'btn btn-lg btn-success btn-block')) }}
                        </div>
                    </div>


                        <!-- Change this to a button or input when using this as a form -->
                    </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop
@extends('...layouts.admin')


@section('title')
New Course
@stop



@section('header')
<h1>New Course</h1>
@stop

@section('content')
<?php
$times = array();

$start = "0:00";
$end = "23:30";

$tStart = strtotime($start);
$tEnd = strtotime($end);
$tNow = $tStart;

while($tNow <= $tEnd){
    $times[date("g:i A",$tNow)] = date("g:i A",$tNow);
    $tNow = strtotime('+30 minutes',$tNow);
}
?>

<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-body">
                {{ Form::open(array('url' => URL::action('CoursesController@store'))) }}
                    <fieldset>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {{ Form::label('name', 'Name') }}
                            {{ Form::text('label', null, array('class' => 'form-control', 'required')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('price', 'Price') }}
                            {{ Form::text('price', null, array('class' => 'form-control' , 'required')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('date', 'Course Date') }}
                            <div class="row">
                                <div class="col-sm-4">
                                    {{ Form::selectMonth('course_month', null, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-sm-2">
                                    {{ Form::selectRange('course_day', '1', '31', null, ['class' => 'form-control']) }}
                                </div>
                                <div class="col-sm-3">
                                    {{ Form::selectYear('course_year', date('Y'), date('Y') + 120, null, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            {{ Form::label('time', 'Course Time') }}
                            {{ Form::select('time', $times,null, array('class' => 'form-control' , 'required')) }}
                        </div>
                        <div class="form-group">
                        {{ Form::label('type', 'Course Type', array('class' => 'control-label', 'for' => 'selectError3')) }}
                            <div class="controls">
                              <select required name="type" id="selectError3" class="form-control">
                                <option value="public">Public</option>
                                <option value="private">Private</option>
                              </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Change Price to</label>
                            {{ Form::text('discount_price', 0, array('class' => 'form-control', 'style' => 'width: 75px; display: inline;')) }}
                            <label class="control-label" style="display: inline">when</label>
                            {{ Form::text('student_discount', 0, array('class' => 'form-control', 'style' => 'width: 50px; display: inline;')) }}
                            <label style="display: inline">students are registering.</label>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            {{ Form::label('location', "Location") }}
                            {{ Form::select('location', ["Cabela's Saskatoon" => "Cabela's Saskatoon", 'RGSL Classroom' => 'RGSL Classroom', "Cabela's Regina" => "Cabela's Regina"], null, array('required', 'class' => 'form-control') ) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('inventory', 'Course Seats') }}
                            {{ Form::text('inventory', null, array('class' => 'form-control', 'required')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('code_type', 'Code Type') }}
                            {{ Form::select('code_type', ['1' => 'Non-Restricted','2' => 'Restricted', '3' => 'Non-Restricted/Restricted'], '1', ['class' => 'form-control', 'required']) }}
                            <span class="label label-danger">{{ $errors->first('code_type') }}</span>
                        </div>
                        <div class="form-group">
                            {{ Form::label('short description', 'Short Description') }}
                            {{ Form::textarea('short_description', null, array('class' => 'form-control')) }}
                        </div>
                    </div>
                        <!-- Change this to a button or input when using this as a form -->
                        {{ Form::submit('Create', array('class' => 'btn btn-lg btn-success btn-block')) }}
                    </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop


@section('footer')
<script>
  $(function() {
    $( "#datepicker" ).datepicker();
  });
  </script>
@stop
@extends('...layouts.admin')


@section('title')
New User
@stop

@section('header')
<h1>New User</h1>
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">New Student</div>
        </div>
            <div class="panel-body">
                {{ Form::open(array('url' => URL::action('StudentsController@store'))) }}
                    <fieldset>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('first_name', 'First Name') }}
                                {{ Form::text('first_name', null, array('class' => 'form-control', 'required')) }}
                                <span class="label label-danger">{{ $errors->first('first_name') }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('middle_name', 'Middle Name') }}
                                {{ Form::text('middle_name', null, array('class' => 'form-control')) }}
                                {{ $errors->first('middle_name') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('last_name', 'Last Name') }}
                                {{ Form::text('last_name', null, array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('last_name') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('email', 'Email') }}
                                {{ Form::text('email', null, array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('email') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('phone', 'Phone Number') }}
                                {{ Form::text('phone', null, array('id' => 'phone', 'class' => 'form-control', 'required')) }}
                                {{ $errors->first('phone') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('birthday', 'Birthday') }}
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::selectMonth('birth_month', null, ['class' => 'form-control']) }}
                                    </div>
                                    <div class="col-sm-2">
                                        {{ Form::selectRange('birth_day', '1', '31', null, ['class' => 'form-control']) }}
                                    </div>
                                    <div class="col-sm-3">
                                        {{ Form::selectYear('birth_year', date('Y') - 17, date('Y') - 120, null, ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('course', 'Course') }}
                                {{ Form::select('course', $courses, null, array( 'id' => 'selectError', 'data-rel' => 'chosen', 'class' => 'form-control', 'required')) }}
                                {{ $errors->first('course') }}
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('address', 'Street Address') }}
                                {{ Form::text('address', null, array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('address') }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('city', 'Town/City') }}
                                {{ Form::text('city', null, array('class' => 'form-control', 'required')) }}
                                <span class="label label-danger">{{ $errors->first('city') }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('state', 'Province/State') }}
                                {{ Form::select('state', $states, 'SK', array('data-rel' => 'chosen', 'class' => 'form-control', 'required')) }}
                                {{ $errors->first('state') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('postal_code', 'Postal Code') }}
                                {{ Form::text('postal_code', null, array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('postal_code') }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('country', 'Country') }}
                                {{ Form::select('country', $countries, 'Canada', array( 'data-rel' => 'chosen','class' => 'form-control','required')) }}
                                {{ $errors->first('country') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('gender', 'Gender') }}
                                {{ Form::select('gender', array('Male' => 'Male', 'Female' => 'Female'), null, array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('gender') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('experience', 'Firearms Experience') }}
                                {{ Form::text('experience', null, array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('experience') }}

                            </div>
                        </div>
                        <!-- Change this to a button or input when using this as a form -->
                        <div style="padding-top: 10px">
                            {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary btn-block', 'required')) }}
                        </div>
                    </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

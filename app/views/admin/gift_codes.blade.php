@extends('layouts.admin')


@section('title')
    Gift Codes
@stop

@section('content')


    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="col-lg-10">
                    <h2>Gift Codes</h2>
                </div>
                <div class="col-lg-2">
                    <a style="padding-top: 2px" href="{{ URL::action('GiftCodeController@create') }}"><button class="btn-sm btn-primary">Create Code</button></a>
                </div>
            </div>
            <div class="panel-body">
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                    <tr>
                        <th>Code</th>
                        <th>Course Type</th>
                        <th>Date Created</th>
                        <th>Date Used</th>
                        <th>Redeemer Email</th>
                        <th>Redeemer Name</th>
                        <th>Course Used</th>
                        <th>Notes</th>
                        <th>Delete</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($codes as $code)
                        <?php $user = true;
                                $co = true;
                        ?>
                            <tr>
                                <td>{{ $code->code }}</td>
                                @if($code->course_type == '1')
                                    <td>Non-Restricted</td>
                                @elseif($code->course_type == '2')
                                    <td>Restricted</td>
                                @elseif($code->course_type == '3')
                                    <td>Non-Restricted/Restricted</td>
                                @endif
                                <td>{{ $code->created_at }}</td>
                                <td>{{ $code->date_redeemed }}</td>

                                @if($user)
                                    <td></td>
                                    <td></td>
                                @endif
                                    @foreach($courses as $course)
                                        @if($code->course_id == $course->id)
                                            <td>{{ $course->label }}</td>
                                            <?php $co = false ?>
                                        @endif
                                    @endforeach
                                @if($co)
                                    <td></td>
                                @endif
                                <td>
                                    {{ $code->note }}
                                    <a href="{{ URL::action('GiftCodeController@edit', [$code->id]) }}"><button type="button" class="btn btn-success pull-right"><i class="fa fa-plus"></i></button></a>
                                </td>
                                <td>
                                    <button type="button" onclick="deleteCode(<?php echo $code->id ?>)" class="btn btn-danger pull-right"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop



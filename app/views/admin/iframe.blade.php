<html>
<head>
    <link href='//fonts.googleapis.com/css?family=Duru+Sans' rel='stylesheet' type='text/css'>
</head>
<body style="background-color: #F6F6F6">
<div style="font-family: 'Duru Sans',Arial,Helvetica,sans-serif;">
    <table style="font-size: 13px;">
        <tbody>
        <?php

        $array = array();

         foreach($courses as $course)
         {
            array_push($array, $course);
         }

        function date_compare($a, $b)
        {
            $t1 = strtotime($a['date']);
            $t2 = strtotime($b['date']);
            return $t1 - $t2;
        }

        usort($array, 'date_compare');

        ?>
        @foreach($array as $course)
            @if($course->type == 'public')
                @if(strtotime($course->date) >= date('U'))
                    <?php
                            $time = strtotime($course['time']);
                            $time = date('G:i', $time+36000);
                    ?>

                    <tr>
                        <td style="color: #747474; font-size: 13px; border-right: 1px #747474 solid; font-family: 'Duru Sans', Arial, Helvetica, sans-serif;">{{ $course['date'] }}</td>
                        <td style="color: #747474; font-size: 13px; border-right: 1px #747474 solid; font-family: 'Duru Sans', Arial, Helvetica, sans-serif;">{{ date('G:i', strtotime($course['time'])) }} - {{ $time }}</td>
                        <td style="color: #747474; font-size: 13px; border-right: 1px #747474 solid; font-family: 'Duru Sans', Arial, Helvetica, sans-serif;">{{ $course['location'] }}</td>
                        <?php if($course['available'] <= 0){
                            echo '<td style="color: #FF0000;"><strong>FULL COURSE</strong></td>';
                        }else if($course['available'] <= 4){
                            echo '<td style="color: #FF9900;"><strong>LIMITED SPACES</strong></td>';
                        }else{
                            echo '<td style="color: #008000;"><strong>OPEN SPACES</strong></td>';
                        } ?>
                        <td style="color:#ff00ff;"><?php $name = $course['label'];
                            $sub = strpos($name, 'Ladies');
                            if($sub === false){

                            }else{
                                echo "<strong>*LADIES ONLY*</strong>";
                            }
                            ?></td>
                    </tr>
                @endif
            @endif
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>

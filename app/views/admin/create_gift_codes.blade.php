@extends('...layouts.admin')


@section('title')
    New User
@stop


@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">New Gift Code</h3>
                </div>
                <div class="panel-body">
                    @if(isset($error))
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endif
                    {{ Form::open(array('url' => URL::action('GiftCodeController@store'))) }}
                    <fieldset>
                        <div class="form-group">
                            {{ Form::label('type', 'Course Type') }}
                            {{ Form::select('type', ['1' => 'Non-Restricted','2' => 'Restricted', '3' => 'Non-Restricted/Restricted'], '1', ['class' => 'form-control', 'required']) }}
                            <span class="label label-danger">{{ $errors->first('type') }}</span>
                        </div>
                        <!-- Change this to a button or input when using this as a form -->
                        {{ Form::submit('Create', array('class' => 'btn btn-lg btn-primary btn-block')) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

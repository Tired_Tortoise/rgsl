@extends('...layouts.admin')


@section('title')
    Courses
@stop




@section('content')
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="col-lg-10">
                    <h2><strong>{{ $course->label }}</strong> Students</h2>
                </div>
            </div>
            <div class="panel-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Remove</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if($students == 0)

                    @else
                        @foreach($students as $student)
                            <tr>
                                <td>
                                    @if(isset($student['FirstName']) && isset($student['LastName']))
                                        {{ $student['FirstName'] . ' ' . $student['LastName'] }}
                                </td>
                                @elseif(isset($student['FirstName']))
                                    {{ $student['FirstName'] }}
                                    </td>
                                @else
                                    <a href="{{ URL::action('StudentsController@edit', $student['Id']) }}"><button class="btn btn-sm btn-info pull-right"><i class="fa fa-edit"></i></button></a>
                                    </td>
                                @endif
                                <td>{{ $student['Email'] }}</td>
                                @if(isset($student['Phone1']))
                                    <td>{{ $student['Phone1'] }}</td>
                                @else
                                    <td></td>
                                @endif
                                <td>
                                    <button type="button" onclick="removeStudent(<?php echo $student['Id'] . ', ' . $course->id ?>)" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script>
        function removeStudent(id, course){
        var url = "<?php echo URL::asset('/remove')  ?>/" + id + '/' + course;
            var result = confirm("Are you sure you want to remove this student from the course? " + url);

            if (result == true)
            {
                $.ajax({
                    url: url,
                    type: 'Delete',
                    success: function(response){
                        location.reload();
                    }
                });
            }
        }
    </script>
@stop
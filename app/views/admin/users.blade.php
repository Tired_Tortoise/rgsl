@extends('...layouts.admin')


@section('title')
Users
@stop

@section('header')
    <a href="{{ URL::to('users/create') }}"><button class="btn btn-primary btn-lg pull-right">Create User</button></a>
    <h1>Users</h1>
@stop

@section('content')
<div class="col-lg-8">
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Email</th>
                        <th>Delete User</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->email }}
                            <a href="{{ URL::action('UsersController@edit', $user->id) }}"><button class="btn btn-sm btn-info pull-right"><i class="fa fa-edit"></i></button></a>
                        </td>
                        <td>
                            {{ Form::open(array('route' => array('users.destroy', $user->id), 'method' => 'delete')) }}
                                <button type="submit" class="btn btn-danger btn-circle">
                                    <i class="fa fa-times"></i>
                                </button>
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-lg-4">
    <div class="panel panel-default">
        <div class="panel-body">
            <a href="{{ URL::action('UsersController@create') }}"><button class="btn-block btn-lg btn-primary">Create User</button></a>
        </div>
    </div>
</div>
@stop


@extends('...layouts.admin')


@section('title')
New User
@stop

@section('header')
<h1>New User</h1>
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">Edit Student</div>
        </div>
            <div class="panel-body">
                {{ Form::open(array('url' => URL::action('StudentsController@update', $student[0]['Id']), 'method' => 'put')) }}
                    <fieldset>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('first_name', 'First Name') }}
                                {{ Form::text('first_name', $student[0]['FirstName'], array('class' => 'form-control', 'required')) }}
                                <span class="label label-danger">{{ $errors->first('first_name') }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('middle_name', 'Middle Name') }}
                                @if(isset($student[0]['MiddleName']))
                                {{ Form::text('middle_name', $student[0]['MiddleName'], array('class' => 'form-control')) }}
                                @else
                                {{ Form::text('middle_name', null, array('class' => 'form-control')) }}
                                @endif
                                {{ $errors->first('middle_name') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('last_name', 'Last Name') }}
                                {{ Form::text('last_name', $student[0]['LastName'], array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('last_name') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('email', 'Email') }}
                                {{ Form::text('email', $student[0]['Email'], array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('email') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('phone', 'Phone Number') }}
                                {{ Form::text('phone', $student[0]['Phone1'], array('id' => 'phone', 'class' => 'form-control', 'required')) }}
                                {{ $errors->first('phone') }}

                            </div>
                            <div class="form-group">
                                {{ Form::label('birthday', 'Birthday') }}
                                <div class="row">
                                    <div class="col-sm-4">
                                        {{ Form::selectMonth('birth_month', date('m', strtotime($student[0]['Birthday'])), ['class' => 'form-control']) }}
                                    </div>
                                    <div class="col-sm-2">
                                        {{ Form::selectRange('birth_day', '1', '31', date('d', strtotime($student[0]['Birthday'])), ['class' => 'form-control']) }}
                                    </div>
                                    <div class="col-sm-3">
                                        {{ Form::selectYear('birth_year', date('Y') - 17, date('Y') - 120, date('Y', strtotime($student[0]['Birthday'])), ['class' => 'form-control']) }}
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                {{ Form::label('course', 'Course') }}
                                {{ Form::select('course', $courses, $current['course_id'], array( 'id' => 'selectError', 'data-rel' => 'chosen', 'class' => 'form-control', 'required')) }}
                                {{ $errors->first('course') }}
                            </div>


                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                {{ Form::label('address', 'Street Address') }}
                                {{ Form::text('address', $student[0]['StreetAddress1'], array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('address') }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('city', 'Town/City') }}
                                {{ Form::text('city', $student[0]['City'], array('class' => 'form-control', 'required')) }}
                                <span class="label label-danger">{{ $errors->first('city') }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('state', 'Province/State') }}
                                {{ Form::select('state', $states, $student[0]['State'], array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('state') }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('postal_code', 'Postal/Zip Code') }}
                                {{ Form::text('postal_code', $student[0]['PostalCode'], array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('postal_code') }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('country', 'Country') }}
                                {{ Form::select('country', $countries, $student[0]['Country'], array( 'data-rel' => 'chosen','class' => 'form-control','required')) }}
                                {{ $errors->first('country') }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('gender', 'Gender') }}
                                {{ Form::select('gender', array('Male' => 'Male', 'Female' => 'Female'), $student[0]['_Gender'], array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('gender') }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('experience', 'Firearms Experience') }}
                                {{ Form::text('experience', $student[0]['_FirearmsExperience'], array('class' => 'form-control', 'required')) }}
                                {{ $errors->first('experience') }}
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <!-- Change this to a button or input when using this as a form -->
                            {{ Form::submit('Save', array('class' => 'btn btn-lg btn-success btn-block')) }}
                        </div>
                        <div class="col-lg-3 pull-right">
                            <button type="button" class="btn btn-block btn-danger btn-lg " onclick="deleteStudent(<?php echo $student[0]['Id'] ?>)">Delete</button>
                        </div>
                    </fieldset>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>
@stop

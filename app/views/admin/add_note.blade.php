@extends('...layouts.admin')


@section('title')
    Add Note
@stop


@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit Gift Code Note</h3>
                </div>
                <div class="panel-body">
                    @if(isset($error))
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endif
                    {{ Form::open(array('url' => URL::action('GiftCodeController@update', $code->id), 'method' => 'put')) }}
                    <fieldset>
                        <div class="form-group">
                            {{ Form::label('type', 'Course Type') }}
                            {{ Form::textarea('note', $code->note, ['class' => 'form-control', 'required']) }}
                            <span class="label label-danger">{{ $errors->first('type') }}</span>
                        </div>
                        <!-- Change this to a button or input when using this as a form -->
                        {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary btn-block')) }}
                    </fieldset>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

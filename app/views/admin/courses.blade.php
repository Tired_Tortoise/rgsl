@extends('...layouts.admin')


@section('title')
Courses
@stop




@section('content')
<div class="col-lg-8">
    <div class="panel panel-default">
        <div class="panel-body">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Type</th>
                        <th>Date</th>
                        <th>Class Size</th>
                        <th>Seats Available</th>
                        <th>Delete Course</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($courses as $course)
                    <tr>
                        <td>{{ $course->id }}</td>
                        <td><a href="{{ URL::action('CoursesController@directory', $course->id) }}">{{ $course->label }}</a> <a href="{{ URL::action('CoursesController@edit', $course->id) }}"><button class="btn btn-sm btn-info pull-right"><i class="fa fa-edit"></i></button></a></td>
                        <td>${{ $course->price }}</td>
                        <td>{{ $course->type }}</td>
                        <td>{{ date("m/d/Y", strtotime($course->date)) }}</td>
                        <td>{{ $course->inventory }}</td>
                        <td>{{ $course->available }}</td>
                        <td>
                            <button type="button" onclick="deleteCourse(<?php echo $course->id ?>)" class="btn btn-danger pull-right"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="col-lg-4">
    <div class="panel panel-default">
            <div class="panel-body">
                <a href="{{ URL::action('CoursesController@create') }}"><button class="btn-block btn-lg btn-primary">Create Courses</button></a>
            </div>
        </div>
    </div>
</div>
@stop
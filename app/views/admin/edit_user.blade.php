@extends('...layouts.admin')


@section('title')
    Edit User
@stop


@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Edit User</h3>
                </div>
                    <div class="panel-body">
                        @if(isset($error))
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endif
                        {{ Form::open(array('url' => URL::action('UsersController@update', $user['id']), 'method' => 'put')) }}
                        <fieldset>
                            <div class="form-group">
                                {{ Form::label('email', 'Email') }}
                                {{ Form::text('email', $user['email'], array('class' => 'form-control', 'required')) }}
                                <span class="label label-danger">{{ $errors->first('email') }}</span>
                            </div>
                            <div class="form-group">
                                {{ Form::label('password', 'Password') }}
                                {{ Form::password('password', array('class' => 'form-control', 'required')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('password2', 'Repeat Password') }}
                                {{ Form::password('password2', array('class' => 'form-control', 'required')) }}
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            {{ Form::submit('Save', array('class' => 'btn btn-lg btn-primary btn-block')) }}
                        </fieldset>
                        {{ Form::close() }}
                    </div>
            </div>
        </div>
    </div>
@stop

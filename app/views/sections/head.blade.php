@section('head')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="CleverAdmin - Bootstrap Admin Template">
    <meta name="author" content="Łukasz Holeczek">
    <meta name="keyword" content="CleverAdmin, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ URL::asset('assets/admin/ico/apple-touch-icon-144-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ URL::asset('assets/admin/ico/apple-touch-icon-114-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ URL::asset('assets/admin/ico/apple-touch-icon-72-precomposed.png') }}">
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="{{ URL::asset('assets/admin/ico/apple-touch-icon-57-precomposed.png') }}">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="//rgsl.ca/favicon-32x32.png" sizes="32x32">

    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('assets/admin/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- page css files -->
    <link href="{{ URL::asset('assets/admin/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('assets/admin/css/jquery-ui.min.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('assets/admin/css/style.min.css') }}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


@show
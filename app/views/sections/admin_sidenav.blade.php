<!-- start: Main Menu -->
<div class="sidebar col-md-2 col-sm-1 ">

    <div class="sidebar-collapse collapse">

        <div class="nav-sidebar title">
            <span>Main Menu</span>
        </div>
        <ul class="nav nav-sidebar">
            <li>
                <a href="{{ URL::to('students') }}">
                    <i class="fa fa-users"></i>
                    <span class="hidden-sm text"> Students</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('courses') }}">
                    <i class="fa fa-book"></i>
                    <span class="hidden-sm text"> Courses</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('giftcodes') }}">
                    <i class="fa fa-gift"></i>
                    <span class="hidden-sm text"> Gift Codes</span>
                </a>
            </li>
            <li>
                <a href="{{ URL::to('users') }}">
                    <i class="fa fa-user"></i>
                    <span class="hidden-sm text"> Users</span>
                </a>
            </li>
        </ul>
    </div>
    <a id="main-menu-min" class="full visible-md visible-lg"><i class="fa fa-angle-double-left"></i></a>
</div>
<!-- end: Main Menu -->
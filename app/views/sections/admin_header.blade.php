<div class="navbar" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar">a</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ URL::to('students') }}"> RGSL</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="index.html#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars" ></i></a>
                <ul class="dropdown-menu">

                    <li><a href="{{ URL::to('logout') }}"><i class="fa fa-lock"></i> Logout</a></li>
                </ul>
            </li>

        </ul>
    </div>
</div>
<!-- end: Header -->
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGiftCodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('gift_codes', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('code');
            $table->integer('course_id');
            $table->integer('user_inf_id');
            $table->date('date_redeemed');
            $table->string('course_type');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('gift_codes');
    }

}

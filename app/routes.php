<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


Route::get('/', 'HomeController@index');

Route::get('step_one', 'HomeController@step_one');

Route::post('step_two', 'HomeController@step_two');

Route::get('step_two', 'HomeController@retryTwo');

Route::get('step_three/{id}', 'HomeController@step_three');

Route::post('step_three/{id}', 'HomeController@step_three');

Route::post('step_four', 'HomeController@step_four');

Route::get('step_four', 'HomeController@retryFour');

Route::post('step_three_edit', 'HomeController@step_three_edit');

Route::post('step_five', 'HomeController@step_five');

Route::post('step_six', 'HomeController@step_six');

Route::get('step_six', 'HomeController@step_six');

Route::get('printable', 'HomeController@printable');

Route::resource('sessions', 'SessionsController');

Route::get('login', 'SessionsController@create');

Route::get('logout', 'SessionsController@destroy');

Route::get('iframe', 'IframeController@index');

Route::get('pass', 'PasswordController@index');

Route::post('pass/recovery', 'PasswordController@sendPass');

Route::get('addCode/{gc}', 'HomeController@addGiftCode');

Route::group(array('before' => 'auth'), function()
{

    Route::resource('users', 'UsersController');

    Route::resource('courses', 'CoursesController');

    Route::get('directory/{id}', 'CoursesController@directory');

    Route::resource('students', 'StudentsController');

    Route::resource('giftcodes', 'GiftCodeController');

    Route::delete('remove/{id}/{course_id}', 'StudentsController@remove');

});


jQuery(function($){
    fields = [
              'phone_number',
              'postal_code',
              'email',
              'number'
    ]

     $.each( fields, function (index, value) {
        $('input.'+value).formance('format_'+value)
                         .addClass('form-control')
                         .wrap('<div class=\'form-group\' />')
                         .parent()
                            .append('<label class="control-label" style="display: none; padding-bottom: 0px"></label>');

        $('input.'+value).on('keyup change blur', function (value) {
            return function (event) {
                $this = $(this);
                if ($this.formance('validate_'+value)) {
                    $this.parent()
                            .removeClass('has-success has-error')
                            .addClass('has-success')
                            .children(':last')
                                .text('Valid!').show();
                    $("input[type='submit']").prop("disabled", false);
                } else {
                    $this.parent()
                            .removeClass('has-success has-error')
                            .addClass('has-error')
                            .children(':last')
                                .text('Invalid').show()
                    $("input[type='submit']").prop("disabled", true);
                }
            }
        }(value));
     });
});

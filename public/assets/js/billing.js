/**
 * Created by user on 1/28/15.
 */

(function() {
    var StripeBilling = {
        init: function() {
            this.form = $('#billing-form');
            this.submitButton = $('#sButton');
            this.submitButtonValue = this.submitButton.val();

            var stripeKey = $('meta[name="publishable-key"]').attr('content');
            Stripe.setPublishableKey(stripeKey);

            this.bindEvents();

        },

        bindEvents: function(){
            this.form.on('submit', $.proxy(this.sendToken, this));
        },

        sendToken: function(event) {
            this.submitButton.val('One Moment').prop('disabled', true);

            Stripe.createToken(this.form, $.proxy(this.stripeResponseHandler, this));

            event.preventDefault();

        },

        stripeResponseHandler: function(status, response) {
            if (response.error) {

                this.form.find('#first-message').hide();

                this.form.find('.payment-errors').show().text(response.error.message + " Please re-check your card data carefully and try again. If this message persists, contact your card issuer for more details.");

                return this.submitButton.prop('disabled', false).val(this.submitButtonValue);

            }

            $('<input>', {
                type: 'hidden',
                name: 'stripe-token',
                value: response.id
            }).appendTo(this.form);

            this.form[0].submit();
        }
    };

    StripeBilling.init();
})();